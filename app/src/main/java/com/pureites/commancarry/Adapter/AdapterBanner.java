package com.pureites.commancarry.Adapter;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pureites.commancarry.Global.Typefaces;
import com.pureites.commancarry.Model.Banner;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;

import com.pureites.commancarry.Global.Global;
import com.pureites.commancarry.Global.SendMail;
import com.pureites.commancarry.Global.SharedPreference;
import com.pureites.commancarry.Global.StaticUtility;
import com.pureites.commancarry.R;

/**
 * Created by divya on 4/8/17.
 */

public class AdapterBanner extends RecyclerView.Adapter<AdapterBanner.Viewholder> {

    Context context;
    ArrayList<Banner> banners = new ArrayList<>();

    public AdapterBanner(Context context, ArrayList<Banner> banners) {
        this.context = context;
        this.banners = banners;
    }

    @Override
    public Viewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = null;
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_benner, viewGroup, false);
        return new AdapterBanner.Viewholder(view);
    }

    @Override
    public void onBindViewHolder(final Viewholder viewholder, int position) {
        Banner banner = banners.get(position);

        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                viewholder.pbImgHolder.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));
            }
        }

        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) viewholder.frameBenner.getLayoutParams();
        if (position == 0) {
            params.setMargins((int) context.getResources().getDimension(R.dimen.benner_marginLeight_Right), 0, 10, 0);
            viewholder.frameBenner.setLayoutParams(params);
        } else if (position == getItemCount() - 1) {
            params.setMargins(10, 0, (int) context.getResources().getDimension(R.dimen.benner_marginLeight_Right), 0);
            viewholder.frameBenner.setLayoutParams(params);
        } else {
            params.setMargins(10, 0, 10, 0);
            viewholder.frameBenner.setLayoutParams(params);
        }

        viewholder.txtBennerName.setText(banner.getTitle());
        Picasso.get().load(banner.getImage_url()).resize(700, 500).centerInside().into(viewholder.imgBenner, new Callback() {
            @Override
            public void onSuccess() {
                viewholder.rlImgHolder.setVisibility(View.GONE);
            }

            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                viewholder.rlImgHolder.setVisibility(View.GONE);
            }
        });

    }

    @Override
    public int getItemCount() {
        return banners.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        ImageView imgBenner;
        TextView txtBennerName;
        FrameLayout frameBenner;
        RelativeLayout rlImgHolder;
        ProgressBar pbImgHolder;

        public Viewholder(View itemView) {
            super(itemView);
            txtBennerName = (TextView) itemView.findViewById(R.id.txtBennerName);
            imgBenner = (ImageView) itemView.findViewById(R.id.imgBenner);
            frameBenner = (FrameLayout) itemView.findViewById(R.id.frameBenner);
            rlImgHolder = (RelativeLayout) itemView.findViewById(R.id.rlImgHolder);
            pbImgHolder = (ProgressBar) itemView.findViewById(R.id.pbImgHolder);
            txtBennerName.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
            txtBennerName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));

        }
    }
}
