package com.pureites.commancarry.Model;

public class Banner {
    String title;
    String image_url;

    public Banner(String title, String image_url) {
        this.title = title;
        this.image_url = image_url;
    }

    public String getTitle() {
        return title;
    }

    public String getImage_url() {
        return image_url;
    }
}
