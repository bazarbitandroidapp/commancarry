package com.pureites.commancarry.Activity;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Filter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.OkHttpResponseAndJSONObjectRequestListener;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.ndk.CrashlyticsNdk;
import com.facebook.FacebookSdk;
import com.facebook.FacebookSdkNotInitializedException;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.pureites.commancarry.Adapter.AdapterBanner;
import com.pureites.commancarry.Adapter.DrawerItemsAdapter;
import com.pureites.commancarry.Fragment.CheckoutFragment;
import com.pureites.commancarry.Fragment.ContactUsFragment;
import com.pureites.commancarry.Fragment.FeedbackFragment;
import com.pureites.commancarry.Fragment.OrderHistoryFragment;
import com.pureites.commancarry.Fragment.ProductListFragment;
import com.pureites.commancarry.Global.Global;
import com.pureites.commancarry.Global.Typefaces;
import com.pureites.commancarry.Linkedin.LISessionManager;
import com.pureites.commancarry.Model.Banner;
import com.pureites.commancarry.Model.CMSPages;
import com.pureites.commancarry.Model.DrawerItem;
import com.pureites.commancarry.Model.ProductList;
import com.pureites.commancarry.Pdk.PDKClient;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import com.pureites.commancarry.Adapter.ViewPagerAdapter;
import com.pureites.commancarry.Fragment.CCAvenueFragment;
import com.pureites.commancarry.Fragment.CMSFragment;
import com.pureites.commancarry.Fragment.CartFragment;
import com.pureites.commancarry.Fragment.MegaMenuCategoryFragment;
import com.pureites.commancarry.Fragment.MyAccountFragment;
import com.pureites.commancarry.Fragment.OrderDetailFragment;
import com.pureites.commancarry.Fragment.PaymentFragment;
import com.pureites.commancarry.Fragment.PlaceOrderSuccessFragment;
import com.pureites.commancarry.Fragment.ProductDetailFragment;
import com.pureites.commancarry.Fragment.WishlistFragment;
import com.pureites.commancarry.Global.SendMail;
import com.pureites.commancarry.Global.SharedPreference;
import com.pureites.commancarry.Global.StaticUtility;
import com.pureites.commancarry.Instagram.InstagramApp;
import com.pureites.commancarry.Model.AddressList;
import com.pureites.commancarry.Model.Category;
import com.pureites.commancarry.Model.ProductItem;
import com.pureites.commancarry.R;

import de.greenrobot.event.EventBus;
import de.hdodenhof.circleimageview.CircleImageView;
import instamojo.library.InstamojoPay;
import instamojo.library.InstapayListener;
import io.fabric.sdk.android.Fabric;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener,
        GoogleApiClient.OnConnectionFailedListener,
        ProductListFragment.OnFragmentInteractionListener,
        ProductDetailFragment.OnFragmentInteractionListener,
        CartFragment.OnFragmentInteractionListener,
        WishlistFragment.OnFragmentInteractionListener,
        OrderHistoryFragment.OnFragmentInteractionListener,
        MyAccountFragment.OnFragmentInteractionListener,
        FeedbackFragment.OnFragmentInteractionListener,
        CMSFragment.OnFragmentInteractionListener, TextWatcher,
        CheckoutFragment.OnFragmentInteractionListener,
        PaymentFragment.OnFragmentInteractionListener,
        PlaceOrderSuccessFragment.OnFragmentInteractionListener,
        MegaMenuCategoryFragment.OnFragmentInteractionListener,
        ContactUsFragment.OnFragmentInteractionListener,
        CCAvenueFragment.OnFragmentInteractionListener,
        DrawerItemsAdapter.DrawerCategoryOption {

    ImageView imageLogo, imageOrderHistoryFilter;
    private static Context context;
    static String picUrl = null;
    private static URL urla = null;
    private static URI urin = null;
    public static String isCheckoutBack = "";
    LinearLayout linearHome, linearCatagory, linearNotification, linearWishlist, linearProfile, llLogout, llLogIn;
    ImageView imageHome, imageCatagory, imageNotification, imageWishlist, imageProfile,
            imageNavigation, imageCartBack, imageCart;
    TextView textHome, textCatagory, textNotification, textWishlist, textProfile, txtCatName;
    TextView txtLogout, txtLogIn;
    ImageView imgLogout, imgLogin;

    public static CircleImageView circularImageViewUser;
    static TextView txtUserName;
    static TextView txtUserEmail;
    CardView cardviewBottomNavigation;

    FragmentManager fragmentManager;
    Bundle bundle = new Bundle();
    Toolbar toolbar;
    TextView txtCardCount;

    FrameLayout frameLayoutCart;
    String qty, wishlistcount;

    LinearLayout mLlBenner;
    LinearLayout llSreach;

    RecyclerView recyclerviewCategories;

    private ProductItem productItem;
    private ArrayList<ProductItem> productItems = new ArrayList<>();

    //region HomeFragment
    ViewPager Slider;
    ArrayList<Category> categories;
    ArrayList<DrawerItem> drawerItems = new ArrayList<>();
    ;
    String[] img;
    int currentPage = 0;
    Timer timer;
    RelativeLayout relativeProgress;
    static RecyclerView recyclerviewBestSelling;
    static RecyclerView recyclerviewMostwanted;
    static RecyclerView recyclerviewNewArrivals;
    RecyclerView recyclerviewBenner;
    TextView txtBestSeller, txtMostwanted, txtNewArrivals;
    LinearLayout llBestSeller, llMostWanted, llArrivals;
    ProgressBar progress;
    //endregion

    private GoogleApiClient mGoogleApiClient;
    private InstagramApp mApp;
    Boolean doubleBackToExitPressedOnce = false;
    private static boolean isDashBoard = true;
    LinearLayout llBottomNavigation, llToolbar, llMenu;

    private EventBus eventBus = EventBus.getDefault();
    private int i = 0;
    private AlertDialog internetAlert;

    String is_silder = "", is_wishlist = "", is_mostwanted = "", is_bestselller = "", is_arrivals = "",
            strorderid= "";

    public static boolean checkback = false;
    public static boolean PlaceOrderSuccessback = false;
    public static boolean ClearSharePreference = false;

    TextView txtWishlistCount;

    JSONArray jsonArrayBenner;
    private TextView mTxtInformation;
    RecyclerView recyclerviewCMS;
    LinearLayout mLlCMS;

    AutoCompleteTextView editSearch;
    LinearLayout llNavigationHeader;

    ArrayList<AddressList> addressLists;
    static String userID = "";

    static DrawerLayout drawer;

    static ImageView imgUser;

    //pinterest
    PDKClient pdkClient;

    String strShippingaddress = null, strShippinguserName = null, strShippingphoneno = null;

    //Deals of day products
    private LinearLayout mLlDealsOfDays;
    private TextView mTxtDealsOfDays;
    private RecyclerView mRvDealsOfDays;

    //Best selling
    static ArrayList<ProductList> bestSellingProductLists;
    private boolean BestSellingLoading = true;
    private boolean BestSellingSetup = true;
    private int mBestSellingLimit = 10, mBestSellingOffset = 0;

    //MostWanted
    static ArrayList<ProductList> MostWantedProductLists;
    private boolean MostWantedLoading = true;
    private boolean MostWantedSetup = true;
    private int mMostWantedLimit = 10, mMostWantedOffset = 0;

    //New Arrival
    static ArrayList<ProductList> NewArrivalProductLists;
    private boolean NewArrivalLoading = true;
    private boolean NewArrivalSetup = true;
    private int mNewArrivalLimit = 10, mNewArrivalOffset = 0;

    //Deal Product
    static ArrayList<ProductList> DealProductLists;
    private boolean DealLoading = true;
    private boolean DealSetup = true;
    private int mDealLimit = 10, mDealOffset = 0;

    private RecyclerView mRvDrawer;

    private LinearLayout mLlFooter;
    private TextView mTxtFollow, mTxtOurProduct;
    private FrameLayout mFlDeal;
    TextView mTxtDealLable, mTxtDeal;
    private TextView mTxtCourierPartner;
    private ImageView mImgFB, mImgGoogle, mImgOurProduct;

    private boolean isRefresh = true;

    private String strDealLable = "";
    private InstapayListener listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FacebookSdk.sdkInitialize(this);
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics(), new CrashlyticsNdk());
        setContentView(R.layout.activity_main);
        context = MainActivity.this;
        eventBus.register(context);

        //Pinterest
        pdkClient = PDKClient.configureInstance(this, StaticUtility.PinterestApp_ID);
        pdkClient.onConnect(this);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mApp = new InstagramApp(this, StaticUtility.CLIENT_ID,
                StaticUtility.CLIENT_SECRET, StaticUtility.CALLBACK_URL);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();


        Initialization();
        OnClickListener();
        TypeFace();
        AppSettings();

        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
            llToolbar.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
            toolbar.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                progress.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));

            }
        }

        changeTextCard(txtCardCount);
        chanageImages(txtWishlistCount);

        imageHome.setImageResource(R.drawable.ic_home);
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
            imageHome.setColorFilter(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
            textHome.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        }


        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.APP_LOGO) != "") {
            String strLOGO = SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.APP_LOGO);

            //region ImageLOGO
            try {
                URL urla = null;
            /*picUrl = picUrl.replace("[", "");
            picUrl = picUrl.replace("]", "").replace("\"", "");*/
                urla = new URL(strLOGO);
                URI urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(), urla.getPath(), urla.getQuery(), urla.getRef());
                picUrl = String.valueOf(urin.toURL());
                // Capture position and set to the ImageView
                Picasso.get()
                        .load(picUrl)
                        .into(imageLogo, new Callback() {
                            @Override
                            public void onSuccess() {
                                //holder.pbHome.setVisibility(View.INVISIBLE);
                            }

                            @Override
                            public void onError(Exception e) {
                                //holder.pbHome.setVisibility(View.INVISIBLE);
                            }
                        });
            } catch (MalformedURLException e) {
                e.printStackTrace();
                //Creating SendMail object
                SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in MainActivity.java When parsing url\n" + e.toString());
                //Executing sendmail to send email
                sm.execute();
            } catch (URISyntaxException e) {
                e.printStackTrace();
                //Creating SendMail object
                SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in MainActivity.java When parsing url\n" + e.toString());
                //Executing sendmail to send email
                sm.execute();
            }
            //endregion
        }


        final CoordinatorLayout coodinator = (CoordinatorLayout) findViewById(R.id.coodinator);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                coodinator.setTranslationX(slideOffset * drawerView.getWidth());
                drawer.bringChildToFront(drawerView);
                drawer.requestLayout();
            }
        };

        drawer.setDrawerListener(toggle);
        toggle.syncState();
        toggle.setDrawerIndicatorEnabled(false);
        imageNavigation.setVisibility(View.VISIBLE);
        imageNavigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.Is_Slider_Active) != null) {
            is_silder = SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.Is_Slider_Active);
        }
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.Is_Wishlist_Active) != null) {
            is_wishlist = SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.Is_Wishlist_Active);
        }
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.Is_Seller_Active) != null) {
            is_bestselller = SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.Is_Seller_Active);
        }
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.Is_Arrival_Active) != null) {
            is_arrivals = SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.Is_Arrival_Active);
        }
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.Is_Mostwanted_Active) != null) {
            is_mostwanted = SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.Is_Mostwanted_Active);
        }

        /*GetCartCount();*/

        getCategory();

        if (is_silder.equals("1")) {
            Silder();
        } else {
            Slider.setVisibility(View.GONE);
        }
        if (is_bestselller.equals("1")) {
            getBestSeller();
        } else {
            llBestSeller.setVisibility(View.GONE);
        }
        if (is_mostwanted.equals("1")) {
            getWantedProduct();
        } else {
            llMostWanted.setVisibility(View.GONE);
        }
        if (is_arrivals.equals("1")) {
            getNewArrivals();
        } else {
            llArrivals.setVisibility(View.GONE);
        }

        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.Is_Wishlist_Active) != null) {
            if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.Is_Wishlist_Active).equals("1")) {
                linearWishlist.setVisibility(View.VISIBLE);
            } else {
                linearWishlist.setVisibility(View.GONE);
            }
        }

        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.Is_Cart_Active) != null) {
            if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.Is_Cart_Active).equals("1")) {
                frameLayoutCart.setVisibility(View.VISIBLE);
            } else {
                frameLayoutCart.setVisibility(View.GONE);
            }
        }

        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                StaticUtility.Is_MEGAMENU_Active) != null) {
            String strIsMegaMenuActive = SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                    StaticUtility.Is_MEGAMENU_Active);
            if (strIsMegaMenuActive.equalsIgnoreCase("1")) {
                linearCatagory.setVisibility(View.VISIBLE);
            } else {
                linearCatagory.setVisibility(View.GONE);
            }
        }

        getBenners();

        recyclerviewBestSelling.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                //position starts at 0
                int lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition();
                int itemCount = layoutManager.getItemCount();
                int count = itemCount - 5;
                if (lastVisibleItemPosition >= layoutManager.getItemCount() - 5) {
                    if (BestSellingLoading) {
                        BestSellingLoading = false;
                        BestSellingSetup = false;
                        getBestSeller();
                    }
                }
            }
        });

        recyclerviewMostwanted.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                int lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition();
                if (lastVisibleItemPosition >= layoutManager.getItemCount() - 5) {
                    if (MostWantedLoading) {
                        MostWantedLoading = false;
                        MostWantedSetup = false;
                        getWantedProduct();
                    }
                }
            }
        });

        recyclerviewNewArrivals.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                int lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition();
                if (lastVisibleItemPosition >= layoutManager.getItemCount() - 5) {
                    if (NewArrivalLoading) {
                        NewArrivalLoading = false;
                        NewArrivalSetup = false;
                        getNewArrivals();
                    }
                }
            }
        });

        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                StaticUtility.DEAL_PRICE_ENABLED) != null) {
            if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                    StaticUtility.DEAL_PRICE_ENABLED).equalsIgnoreCase("1")) {
                if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.DEAL) != null) {
                    mFlDeal.setVisibility(View.VISIBLE);
                    mTxtDeal.setText(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.DEAL));
                } else {
                    mFlDeal.setVisibility(View.GONE);
                }
                getDealProduct();
            }
        }

        mRvDealsOfDays.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                int lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition();
                if (lastVisibleItemPosition >= layoutManager.getItemCount() - 5) {
                    if (DealLoading) {
                        DealLoading = false;
                        DealSetup = false;
                        getDealProduct();
                    }
                }
            }
        });
    }

    //region OnClickListener
    private void OnClickListener() {
        linearHome.setOnClickListener(this);
        linearCatagory.setOnClickListener(this);
        linearNotification.setOnClickListener(this);
        linearWishlist.setOnClickListener(this);
        linearProfile.setOnClickListener(this);
        llLogout.setOnClickListener(this);
        llLogIn.setOnClickListener(this);
        frameLayoutCart.setOnClickListener(this);
        mImgFB.setOnClickListener(this);
        mImgGoogle.setOnClickListener(this);
        mImgOurProduct.setOnClickListener(this);
        editSearch.addTextChangedListener(this);
    }
    //endregion

    //region Initialization
    private void Initialization() {
        imageLogo = (ImageView) findViewById(R.id.imageLogo);
        linearHome = (LinearLayout) findViewById(R.id.linearHome);
        imgUser = (ImageView) findViewById(R.id.imgUser);
        linearCatagory = (LinearLayout) findViewById(R.id.linearCatagory);
        linearNotification = (LinearLayout) findViewById(R.id.linearNotification);
        linearWishlist = (LinearLayout) findViewById(R.id.linearWishlist);
        linearProfile = (LinearLayout) findViewById(R.id.linearProfile);

        llLogout = (LinearLayout) findViewById(R.id.llLogout);
        llLogIn = (LinearLayout) findViewById(R.id.llLogIn);
        llSreach = (LinearLayout) findViewById(R.id.llSreach);

        txtLogout = (TextView) findViewById(R.id.txtLogout);
        txtLogIn = (TextView) findViewById(R.id.txtLogIn);
        imgLogout = (ImageView) findViewById(R.id.imgLogout);
        imgLogin = (ImageView) findViewById(R.id.imgLogin);

        imageHome = (ImageView) findViewById(R.id.imageHome);
        imageCatagory = (ImageView) findViewById(R.id.imageCatagory);
        imageNotification = (ImageView) findViewById(R.id.imageNotification);
        imageWishlist = (ImageView) findViewById(R.id.imageWishlist);
        imageProfile = (ImageView) findViewById(R.id.imageProfile);

        imageNavigation = (ImageView) findViewById(R.id.imageNavigation);
        imageCartBack = (ImageView) findViewById(R.id.imageCartBack);
        imageCart = (ImageView) findViewById(R.id.imageCart);
        imageOrderHistoryFilter = (ImageView) findViewById(R.id.imageOrderHistoryFilter);
        txtCatName = (TextView) findViewById(R.id.txtCatName);

        imageNavigation.setImageResource(R.drawable.ic_menu);

        textHome = (TextView) findViewById(R.id.textHome);
        textCatagory = (TextView) findViewById(R.id.textCatagory);
        textNotification = (TextView) findViewById(R.id.textNotification);
        textWishlist = (TextView) findViewById(R.id.textWishlist);
        textProfile = (TextView) findViewById(R.id.textProfile);

        llBottomNavigation = (LinearLayout) findViewById(R.id.llBottomNavigation);
        progress = (ProgressBar) findViewById(R.id.progress);
        txtCardCount = (TextView) findViewById(R.id.txtCardCount);
        frameLayoutCart = (FrameLayout) findViewById(R.id.frameLayoutCart);

        circularImageViewUser = (CircleImageView) findViewById(R.id.circularImageViewUser);
        txtUserName = (TextView) findViewById(R.id.txtUserName);
        txtUserEmail = (TextView) findViewById(R.id.txtUserEmail);
        txtWishlistCount = (TextView) findViewById(R.id.txtWishlistCount);
        recyclerviewCMS = (RecyclerView) findViewById(R.id.recyclerviewCMS);
        mTxtInformation = (TextView) findViewById(R.id.txtInformation);

        cardviewBottomNavigation = (CardView) findViewById(R.id.cardviewBottomNavigation);
        llNavigationHeader = (LinearLayout) findViewById(R.id.llNavigationHeader);

        //region HomeFragment
        Slider = (ViewPager) findViewById(R.id.Slider);
        relativeProgress = (RelativeLayout) findViewById(R.id.relativeProgress);

        recyclerviewBestSelling = (RecyclerView) findViewById(R.id.recyclerviewBestSelling);
        recyclerviewMostwanted = (RecyclerView) findViewById(R.id.recyclerviewMostwanted);
        recyclerviewNewArrivals = (RecyclerView) findViewById(R.id.recyclerviewNewArrivals);
        recyclerviewBenner = (RecyclerView) findViewById(R.id.recyclerviewBenner);
        recyclerviewCategories = (RecyclerView) findViewById(R.id.recyclerviewCategories);
        mRvDealsOfDays = (RecyclerView) findViewById(R.id.rvDealsOfDays);

        txtBestSeller = (TextView) findViewById(R.id.txtBestSeller);
        txtMostwanted = (TextView) findViewById(R.id.txtMostwanted);
        txtNewArrivals = (TextView) findViewById(R.id.txtNewArrivals);
        mTxtDealsOfDays = (TextView) findViewById(R.id.txtDealsOfDays);

        mLlDealsOfDays = (LinearLayout) findViewById(R.id.llDealsOfDays);
        llBestSeller = (LinearLayout) findViewById(R.id.llBestSeller);
        llMostWanted = (LinearLayout) findViewById(R.id.llMostWanted);
        llArrivals = (LinearLayout) findViewById(R.id.llArrivals);
        llToolbar = (LinearLayout) findViewById(R.id.llToolbar);

        /*frameLayoutBestSeller = (FrameLayout) findViewById(R.id.frameLayoutBestSeller);
        framLayoutmostWanted = (FrameLayout) findViewById(R.id.framLayoutmostWanted);
        frameLayoutNewArrivals = (FrameLayout) findViewById(R.id.frameLayoutNewArrivals);*/
        mLlBenner = (LinearLayout) findViewById(R.id.llBenner);
        mRvDrawer = (RecyclerView) findViewById(R.id.rvDrawer);

        editSearch = (AutoCompleteTextView) findViewById(R.id.editSearch);
        editSearch.setThreshold(0);
        mTxtDeal = (TextView) findViewById(R.id.txtDeal);
        mTxtDealLable = (TextView) findViewById(R.id.txtDealLable);
        mLlCMS = (LinearLayout) findViewById(R.id.llCMS);
        mTxtCourierPartner = (TextView) findViewById(R.id.txtCourierPartner);
        mImgFB = (ImageView) findViewById(R.id.imgFB);
        mImgGoogle = (ImageView) findViewById(R.id.imgGoogle);
        mImgOurProduct = (ImageView) findViewById(R.id.imgOurProduct);
        mLlFooter = (LinearLayout) findViewById(R.id.llFooter);
        mFlDeal = (FrameLayout) findViewById(R.id.flDeal);
        mTxtFollow = (TextView) findViewById(R.id.txtFollow);
        mTxtOurProduct = (TextView) findViewById(R.id.txtOurProduct);
        //endregion

    }
    //endregion

    //region AppSettings
    private void AppSettings() {
        txtBestSeller.setTextColor(getResources().getColor(R.color.white));
        txtBestSeller.setBackgroundColor(Color.parseColor("#613f99"));
        txtMostwanted.setTextColor(getResources().getColor(R.color.white));
        txtMostwanted.setBackgroundColor(Color.parseColor("#613f99"));
        mTxtDealsOfDays.setBackgroundColor(Color.parseColor("#613f99"));
        mTxtDealsOfDays.setTextColor(getResources().getColor(R.color.white));
        txtNewArrivals.setTextColor(getResources().getColor(R.color.white));
        txtNewArrivals.setBackgroundColor(Color.parseColor("#613f99"));
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor) != null) {
            editSearch.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        }

        mTxtFollow.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        mTxtOurProduct.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));

        mImgFB.setColorFilter(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        mImgGoogle.setColorFilter(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        mTxtCourierPartner.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        mFlDeal.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        mTxtDealLable.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        mTxtDealLable.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        mTxtDeal.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        txtCatName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        txtUserName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        txtUserEmail.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        txtWishlistCount.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        mTxtInformation.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
//        txtWishlistCount.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TEXTCOLOR)));

        llSreach.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));


        txtLogout.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        txtLogIn.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        llLogout.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        llLogIn.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        imgLogout.setColorFilter(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        imgLogin.setColorFilter(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));

    }
    //endregion

    //region TypeFace
    private void TypeFace() {
        txtBestSeller.setTypeface(Typefaces.TypefaceCalibri_bold(context));
        txtMostwanted.setTypeface(Typefaces.TypefaceCalibri_bold(context));
        mTxtDealsOfDays.setTypeface(Typefaces.TypefaceCalibri_bold(context));
        txtNewArrivals.setTypeface(Typefaces.TypefaceCalibri_bold(context));
        txtUserEmail.setTypeface(Typefaces.TypefaceCalibri_bold(context));
        txtUserName.setTypeface(Typefaces.TypefaceCalibri_bold(context));
        txtLogout.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        txtLogIn.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        txtWishlistCount.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        editSearch.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        mTxtInformation.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        mTxtCourierPartner.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        mTxtDealLable.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        mTxtDeal.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        mTxtFollow.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        mTxtOurProduct.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
    }
    //endregion

    //region movetofragment
    public void movetofragment(Bundle bundle, Fragment fragment) {
        try {
            fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//            fragmentManager.popBackStack();
            fragment.setArguments(bundle);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.replace(R.id.framelayout, fragment);
            fragmentTransaction.commit();
        } catch (IllegalStateException i) {
            i.printStackTrace();
          /*  //Creating SendMail object
            SendMail sm = new SendMail(this, Global.TOEMAIL, Global.SUBJECT, "Getting error in MainActivity.java When call methos move to fragmen.\n" + i.toString());
            //Executing sendmail to send email
            sm.execute();*/
        }
    }
    //endregion

    //region manageBackPress
    public static void manageBackPress(boolean value) {
        if (value) {
            isDashBoard = true;
        } else {
            isDashBoard = false;
        }
    }
    //endregion

    //region chanageTextCard
    public void changeTextCard(TextView textView) {
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
            textView.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        }
// textView.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.BTNTEXTCOLOR)));

        textView.setBackgroundResource(R.drawable.bg_card);
        GradientDrawable gd = (GradientDrawable) textView.getBackground().getCurrent();
        gd.setShape(GradientDrawable.OVAL);
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor) != "") {
            gd.setColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        }
//        gd.setCornerRadii(new float[]{50, 50, 50, 50, 50, 50, 50, 50});
    }
    //endregion

    //region chanageImages
    public void chanageImages(TextView textView) {
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor) != "") {
            textView.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        }
//        button.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TEXTCOLOR)));
        textView.setBackgroundResource(R.drawable.bg_cricler);
        GradientDrawable gd = (GradientDrawable) textView.getBackground().getCurrent();
        gd.setShape(GradientDrawable.OVAL);
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
            gd.setColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        }
//        gd.setCornerRadii(new float[]{50, 50, 50, 50, 50, 50, 50, 50});
//        gd.setCornerRadius(200);
        /* gd.setStroke(2, Color.parseColor(StaticUtility.BORDERCOLOR));*/
    }
    //endregion

    //region Drawer
    public static void manageDrawer(boolean value) {
        if (value) {
            drawer.openDrawer(GravityCompat.START);
        } else {
            drawer.closeDrawer(GravityCompat.START);
        }
    }
    //endregion

    @Override
    public void gotoPaymentGatewayInstamojo(String orderid, String amount, String Fanme, String phoneNo) {
        strorderid = orderid;
        String strEmail = SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USER_EMAIL);
        callInstamojoPay(strEmail, phoneNo, amount, "placeorder", Fanme);
    }
    private void callInstamojoPay(String email, String phone, String amount, String purpose, String buyername) {
        final Activity activity = this;
        InstamojoPay instamojoPay = new InstamojoPay();
        IntentFilter filter = new IntentFilter("ai.devsupport.instamojo");
        registerReceiver(instamojoPay, filter);
        JSONObject pay = new JSONObject();
        try {
            pay.put("email", email);
            pay.put("phone", phone);
            pay.put("purpose", purpose);
            pay.put("amount", amount);
            pay.put("name", buyername);
            pay.put("send_sms", true);
            pay.put("send_email", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        initListener();
        instamojoPay.start(activity, pay, listener);
    }
    private void initListener() {
        listener = new InstapayListener() {
            @Override
            public void onSuccess(String response) {
                String strStatus = "", strTransctionId = "", strPaymentId = "", strToken = "";
                String[] separated = response.split(":");
                strStatus = separated[0]; // this will contain "Fruit"
                strTransctionId = separated[1];
                strPaymentId = separated[3];
                strToken = separated[4];
                String[] separatedStatus = strStatus.split("=");
                strStatus = separatedStatus[1];
                String[] separatedTransction = strPaymentId.split("=");
                strTransctionId = separatedTransction[1];
                PaymentStatusUpdate(strStatus, strorderid, strTransctionId);
                Toast.makeText(context, "Payment Successful: ", Toast.LENGTH_SHORT).show();
                bundle = new Bundle();
                PlaceOrderSuccessFragment placeOrderSuccessFragment = new PlaceOrderSuccessFragment();
                bundle.putString("payment_method", "2");
                bundle.putString("urlStatus", "Transaction Successfully!");
                movetofragment(bundle, placeOrderSuccessFragment);
            }
            @Override
            public void onFailure(int code, String reason) {
                Toast.makeText(context, "Payment Failed: " + reason, Toast.LENGTH_SHORT).show();
                PaymentStatusUpdate("failed", strorderid, "");
                bundle = new Bundle();
                PlaceOrderSuccessFragment placeOrderSuccessFragment = new PlaceOrderSuccessFragment();
                bundle.putString("payment_method", "2");
                bundle.putString("urlStatus", "Transaction Cancelled!");
                movetofragment(bundle, placeOrderSuccessFragment);
            }
        };
    }
    //endregion

    //region FOR Payment Status Update API..
    private void PaymentStatusUpdate(String status, String orderid, String transtionid) {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {"status", "udf1", "txnid"};
        String[] val = {status, orderid, transtionid};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.PaymentStatusUpdate);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers1(context));

        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in MainActivity.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    @Override
    public void onBackPressed() {
        if (checkback) {
            checkback = false;
            String status = "Transaction Cancelled!";
            bundle = new Bundle();
            PlaceOrderSuccessFragment placeOrderSuccessFragment = new PlaceOrderSuccessFragment();
            bundle.putString("payment_method", "2");
            bundle.putString("urlStatus", status);
            movetofragment(bundle, placeOrderSuccessFragment);
        } else {
            if (!isDashBoard) {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    txtCatName.setVisibility(View.GONE);
                    imageCartBack.setVisibility(View.GONE);
                    imageOrderHistoryFilter.setVisibility(View.GONE);

                    imageNavigation.setVisibility(View.VISIBLE);
                    imageLogo.setVisibility(View.VISIBLE);
                    if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.Is_Cart_Active) != null) {
                        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.Is_Cart_Active).equals("1")) {
                            frameLayoutCart.setVisibility(View.VISIBLE);
                        } else {
                            frameLayoutCart.setVisibility(View.GONE);
                        }
                    }
                    llBottomNavigation.setVisibility(View.VISIBLE);
                    cardviewBottomNavigation.setVisibility(View.VISIBLE);
                    toolbar.setVisibility(View.VISIBLE);


                    if (isCheckoutBack.equalsIgnoreCase("Checkout")) {
                        SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                        SharedPreference.ClearPreference(context, Global.Billing_Preference);
                    }

                    GetCartCount();

                    editSearch.setText("");
                    isDashBoard = true;
                    super.onBackPressed();
                    if (ClearSharePreference) {
                        ClearSharePreference = false;
                        SharedPreference.ClearPreference(context, Global.IsClickPreference);
                    }
                }
            } else {
                if (PlaceOrderSuccessback) {
                    PlaceOrderSuccessback = false;
                    PlaceOrderSuccessFragment.isclickback = false;
                    Intent intent = new Intent(this, MainActivity.class);
                    startActivity(intent);
                    finish();
                    isDashBoard = true;
                } else {
                    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                    if (drawer.isDrawerOpen(GravityCompat.START)) {
                        drawer.closeDrawer(GravityCompat.START);
                    } else {
                        //region exit on backProcess
                        if (doubleBackToExitPressedOnce) {
                            System.exit(0);
                        }
                        this.doubleBackToExitPressedOnce = true;
                        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

                        new Handler().postDelayed(new Runnable() {

                            @Override
                            public void run() {
                                doubleBackToExitPressedOnce = false;
                            }
                        }, 3000);
                        //endregion
                    }
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View v) {
        Bundle bundle = new Bundle();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        switch (v.getId()) {
            case R.id.linearHome:
                imageHome.setColorFilter(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                imageCatagory.setColorFilter(Color.parseColor("#919191"));
                imageWishlist.setColorFilter(Color.parseColor("#919191"));
                imageNotification.setColorFilter(Color.parseColor("#919191"));
                imageProfile.setColorFilter(Color.parseColor("#919191"));

                textHome.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                textNotification.setTextColor(Color.parseColor("#919191"));
                textCatagory.setTextColor(Color.parseColor("#919191"));
                textWishlist.setTextColor(Color.parseColor("#919191"));
                textProfile.setTextColor(Color.parseColor("#919191"));

                startActivity(new Intent(context, MainActivity.class));
                finish();
                isDashBoard = true;

                break;
            case R.id.linearCatagory:
                imageHome.setColorFilter(Color.parseColor("#919191"));
                imageCatagory.setColorFilter(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                imageWishlist.setColorFilter(Color.parseColor("#919191"));
                imageNotification.setColorFilter(Color.parseColor("#919191"));
                imageProfile.setColorFilter(Color.parseColor("#919191"));

                textHome.setTextColor(Color.parseColor("#919191"));
                textNotification.setTextColor(Color.parseColor("#919191"));
                textCatagory.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                textWishlist.setTextColor(Color.parseColor("#919191"));
                textProfile.setTextColor(Color.parseColor("#919191"));

                MegaMenuCategoryFragment megaMenuCategoryFragment1 = new MegaMenuCategoryFragment();
                movetofragment(bundle, megaMenuCategoryFragment1);
                break;

            case R.id.linearNotification:
                imageHome.setColorFilter(Color.parseColor("#919191"));
                imageCatagory.setColorFilter(Color.parseColor("#919191"));
                imageWishlist.setColorFilter(Color.parseColor("#919191"));
                imageNotification.setColorFilter(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                imageProfile.setColorFilter(Color.parseColor("#919191"));

                textHome.setTextColor(Color.parseColor("#919191"));
                textNotification.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                textCatagory.setTextColor(Color.parseColor("#919191"));
                textWishlist.setTextColor(Color.parseColor("#919191"));
                textProfile.setTextColor(Color.parseColor("#919191"));
                break;

            case R.id.linearWishlist:
                imageHome.setColorFilter(Color.parseColor("#919191"));
                imageCatagory.setColorFilter(Color.parseColor("#919191"));
                imageWishlist.setColorFilter(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                imageNotification.setColorFilter(Color.parseColor("#919191"));
                imageProfile.setColorFilter(Color.parseColor("#919191"));

                textHome.setTextColor(Color.parseColor("#919191"));
                textNotification.setTextColor(Color.parseColor("#919191"));
                textCatagory.setTextColor(Color.parseColor("#919191"));
                textWishlist.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                textProfile.setTextColor(Color.parseColor("#919191"));
                if (SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID) != null) {
                    userID = SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID);
                    if (!userID.equalsIgnoreCase("")) {
                        WishlistFragment wishlistFragment = new WishlistFragment();
                        movetofragment(bundle, wishlistFragment);
                    } else {
                        imageWishlist.setColorFilter(Color.parseColor("#919191"));
                        imageCatagory.setColorFilter(Color.parseColor("#919191"));
                        imageHome.setColorFilter(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                        imageNotification.setColorFilter(Color.parseColor("#919191"));
                        imageProfile.setColorFilter(Color.parseColor("#919191"));

                        textWishlist.setTextColor(Color.parseColor("#919191"));
                        textNotification.setTextColor(Color.parseColor("#919191"));
                        textCatagory.setTextColor(Color.parseColor("#919191"));
                        textHome.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                        textProfile.setTextColor(Color.parseColor("#919191"));

                        Toast.makeText(context, getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                        SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                        Intent intent = new Intent(context, LoginActivity.class);
                        SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                        SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                        SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                        SharedPreference.ClearPreference(context, Global.Billing_Preference);
                        SharedPreference.ClearPreference(context, Global.ISCheck);
                        startActivity(intent);
                        /*finish();*/
                    }
                } else {
                    imageWishlist.setColorFilter(Color.parseColor("#919191"));
                    imageCatagory.setColorFilter(Color.parseColor("#919191"));
                    imageHome.setColorFilter(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                    imageNotification.setColorFilter(Color.parseColor("#919191"));
                    imageProfile.setColorFilter(Color.parseColor("#919191"));

                    textWishlist.setTextColor(Color.parseColor("#919191"));
                    textNotification.setTextColor(Color.parseColor("#919191"));
                    textCatagory.setTextColor(Color.parseColor("#919191"));
                    textHome.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                    textProfile.setTextColor(Color.parseColor("#919191"));

                    Toast.makeText(context, getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                    SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                    Intent intent = new Intent(context, LoginActivity.class);
                    SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                    SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                    SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                    SharedPreference.ClearPreference(context, Global.Billing_Preference);
                    SharedPreference.ClearPreference(context, Global.ISCheck);
                    startActivity(intent);
                }
                break;

            case R.id.linearProfile:
                imageHome.setColorFilter(Color.parseColor("#919191"));
                imageCatagory.setColorFilter(Color.parseColor("#919191"));
                imageWishlist.setColorFilter(Color.parseColor("#919191"));
                imageNotification.setColorFilter(Color.parseColor("#919191"));
                imageProfile.setColorFilter(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));

                textHome.setTextColor(Color.parseColor("#919191"));
                textNotification.setTextColor(Color.parseColor("#919191"));
                textCatagory.setTextColor(Color.parseColor("#919191"));
                textWishlist.setTextColor(Color.parseColor("#919191"));
                textProfile.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                if (SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID) != null) {
                    userID = SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID);
                    if (!userID.equalsIgnoreCase("")) {
                        MyAccountFragment myAccountFragment = new MyAccountFragment();
                        movetofragment(bundle, myAccountFragment);
                    } else {
                        imageHome.setColorFilter(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                        textHome.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));

                        imageProfile.setColorFilter(Color.parseColor("#919191"));
                        imageCatagory.setColorFilter(Color.parseColor("#919191"));
                        imageWishlist.setColorFilter(Color.parseColor("#919191"));
                        imageNotification.setColorFilter(Color.parseColor("#919191"));

                        textProfile.setTextColor(Color.parseColor("#919191"));
                        textNotification.setTextColor(Color.parseColor("#919191"));
                        textCatagory.setTextColor(Color.parseColor("#919191"));
                        textWishlist.setTextColor(Color.parseColor("#919191"));

                        Toast.makeText(context, getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                        SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                        Intent intent = new Intent(context, LoginActivity.class);
                        SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                        SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                        SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                        SharedPreference.ClearPreference(context, Global.Billing_Preference);
                        SharedPreference.ClearPreference(context, Global.ISCheck);
                        startActivity(intent);
                        /*finish();*/
                    }
                } else {
                    imageHome.setColorFilter(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                    textHome.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));

                    imageProfile.setColorFilter(Color.parseColor("#919191"));
                    imageCatagory.setColorFilter(Color.parseColor("#919191"));
                    imageWishlist.setColorFilter(Color.parseColor("#919191"));
                    imageNotification.setColorFilter(Color.parseColor("#919191"));

                    textProfile.setTextColor(Color.parseColor("#919191"));
                    textNotification.setTextColor(Color.parseColor("#919191"));
                    textCatagory.setTextColor(Color.parseColor("#919191"));
                    textWishlist.setTextColor(Color.parseColor("#919191"));

                    Toast.makeText(context, getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                    SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                    Intent intent = new Intent(context, LoginActivity.class);
                    SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                    SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                    SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                    SharedPreference.ClearPreference(context, Global.Billing_Preference);
                    SharedPreference.ClearPreference(context, Global.ISCheck);
                    startActivity(intent);
                }
                break;

            case R.id.llLogout:
                LISessionManager.getInstance(getApplicationContext()).clearSession();
                pdkClient.getInstance().logout();
                if (mApp.hasAccessToken()) {
                    mApp.resetAccessToken();
                }
                try {
                    if (FacebookSdk.isInitialized()) {
                        LoginManager.getInstance().logOut();
                    }
                } catch (FacebookSdkNotInitializedException e) {
                    e.printStackTrace();
                }
                if (mGoogleApiClient.isConnected()) {
                    Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                            new ResultCallback<Status>() {
                                @Override
                                public void onResult(Status status) {
                                    //updateUI(false);
                                }
                            });
                }
                Logout();
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                }
                break;

            case R.id.llLogIn:
                startActivity(new Intent(context, LoginActivity.class));
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                }
                /*finish();*/
                break;

            case R.id.frameLayoutCart:
                bundle = new Bundle();
                bundle.putString("ActivityType", "MainActivity");
                CartFragment cartFragment = new CartFragment();
                movetofragment(bundle, cartFragment);
                break;

            case R.id.imgFB:
                openFacebookApp();
                break;
            case R.id.imgGoogle:
                openGPlus("104881024939363643737");
                break;

            case R.id.imgOurProduct:
                openOurProduct();
                break;
        }
    }

    //region ViewPager..
    private void getViewPager(final ViewPager viewPager) {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(context, null, img);
        viewPager.setAdapter(viewPagerAdapter);

        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == img.length) {
                    currentPage = 0;
                }
                viewPager.setCurrentItem(currentPage++, true);
            }
        };

        timer = new Timer(); // This will create a new Thread
        timer.schedule(new TimerTask() { // task to be scheduled

            @Override
            public void run() {
                handler.post(Update);
            }
        }, 500, 5000);
    }
    //endregion

    //region FOR silder API..
    private void Silder() {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {};
        String[] val = {};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL + StaticUtility.GETSLIDER);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                try {
                    Log.d("Test", "onResponse object : " + response.toString());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            /*itemVendorStocks.clear();*/
                            relativeProgress.setVisibility(View.GONE);
                        }
                    });
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
//                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                    JSONObject jsonObjectslider = response.getJSONObject("payload");
                                    if (jsonObjectslider.has("slides")) {
                                        Object object = jsonObjectslider.get("slides");
                                        if (!object.equals("")) {
                                            Slider.setVisibility(View.GONE);
                                            JSONArray jsonArrayslider = jsonObjectslider.getJSONArray("slides");
                                            img = new String[1];
                                            JSONObject jsonObject = jsonArrayslider.getJSONObject(0);
                                            JSONArray array = jsonObject.getJSONArray("image_url");
                                            img[i] = String.valueOf(array.get(0));
                                           /* img = new String[jsonArrayslider.length()];
                                           for (int i = 0; i < jsonArrayslider.length(); i++) {
                                                JSONObject jsonObject = jsonArrayslider.getJSONObject(i);
                                                JSONArray array = jsonObject.getJSONArray("image_url");
                                                img[i] = String.valueOf(array.get(0));
                                            }*/
                                            getViewPager(Slider);
                                        } else {
                                            Slider.setVisibility(View.GONE);
                                        }
                                    } else {
                                        Slider.setVisibility(View.GONE);
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in HomeFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region FOR getBestSeller API..
    private void getBestSeller() {
        if (BestSellingSetup) {
            relativeProgress.setVisibility(View.VISIBLE);
        }
        String[] key;
        String[] val;

        String user_id = SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID);
        if (SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID) != null) {
            key = new String[]{"user_id", "offset", "limit"};
            val = new String[]{user_id, String.valueOf(mBestSellingOffset),
                    String.valueOf(mBestSellingLimit)};
        } else {
            key = new String[]{"offset", "limit"};
            val = new String[]{String.valueOf(mBestSellingOffset),
                    String.valueOf(mBestSellingLimit)};
        }

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.getBestSeller);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                try {
                    Log.d("Test", "onResponse object : " + response.toString());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            /*itemVendorStocks.clear();*/
                            relativeProgress.setVisibility(View.GONE);
                        }
                    });
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    llBestSeller.setVisibility(View.VISIBLE);
                                    JSONObject jsonObjectPayload = response.getJSONObject("payload");
                                    if (jsonObjectPayload.has("bestseller")) {
                                        String image = "";
                                        JSONArray jsonArraySeller = jsonObjectPayload.getJSONArray("bestseller");
                                        if (jsonArraySeller.length() > 0) {
                                            if (BestSellingSetup) {
                                                bestSellingProductLists = new ArrayList<>();
                                            }
                                            for (int i = 0; i < jsonArraySeller.length(); i++) {
                                                JSONObject jsonObject = jsonArraySeller.getJSONObject(i);
                                                String productName = jsonObject.getString("name");
                                                String ProductSlug = jsonObject.getString("slug");
                                                String BasePrice = jsonObject.getString("price");
                                                String SalePrice = jsonObject.getString("sale_price");
                                                String product_id = jsonObject.getString("product_id");
                                                String exists_in_wishlist = jsonObject.getString("exists_in_wishlist");
                                                String strRating = jsonObject.getString("avg_rating");
                                                Object objImageURL = jsonObject.get("main_image");
                                                if (!objImageURL.equals("")) {
                                                    JSONObject jsonObjectImages = jsonObject.getJSONObject("main_image");
                                                    image = jsonObjectImages.getString("main_image");
                                                }
                                                bestSellingProductLists.add(new ProductList(productName, BasePrice, SalePrice,
                                                        product_id, image, ProductSlug, exists_in_wishlist, strRating));
                                            }

                                            if (BestSellingSetup) {
                                                BestSellingLoading = true;
                                                mBestSellingOffset = mBestSellingOffset + mBestSellingLimit;
                                                AdapterBestSeller adapterHomeProduct = new AdapterBestSeller(context, bestSellingProductLists);
                                                recyclerviewBestSelling.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                                                recyclerviewBestSelling.setAdapter(adapterHomeProduct);
                                            } else {
                                                if (jsonArraySeller.length() == mBestSellingLimit) {
                                                    mBestSellingOffset = mBestSellingOffset + mBestSellingLimit;
                                                    BestSellingLoading = true;
                                                } else {
                                                    BestSellingLoading = false;
                                                }
                                                recyclerviewBestSelling.getAdapter().notifyDataSetChanged();
                                            }
                                        } else {
                                            if (BestSellingSetup) {
                                                recyclerviewBestSelling.setVisibility(View.GONE);
                                            }
                                        }
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in HomeFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region AdapterBestSeller
    public class AdapterBestSeller extends RecyclerView.Adapter<AdapterBestSeller.Viewholder> {

        Context context;
        JSONArray jsonArray;
        ArrayList<ProductList> productLists;

        public AdapterBestSeller(Context context, JSONArray jsonArray, String type) {
            this.context = context;
            this.jsonArray = jsonArray;
        }

        public AdapterBestSeller(Context context, ArrayList<ProductList> productLists) {
            this.context = context;
            this.productLists = productLists;
        }

        @Override
        public AdapterBestSeller.Viewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = null;
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_product, viewGroup, false);
            return new AdapterBestSeller.Viewholder(view);
        }

        @Override
        public void onBindViewHolder(final Viewholder viewholder, final int position) {
            try {
                if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        viewholder.pbImgHolder.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));
                    }
                }

                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) viewholder.flProducts.getLayoutParams();
                if (position == 0) {
                    params.setMargins(15, 0, 10, 0);
                    viewholder.flProducts.setLayoutParams(params);
                } else if (position == getItemCount() - 1) {
                    params.setMargins(10, 0, 15, 0);
                    viewholder.flProducts.setLayoutParams(params);
                } else {
                    params.setMargins(10, 0, 10, 0);
                    viewholder.flProducts.setLayoutParams(params);
                }

                final ProductList productList = productLists.get(position);
                String strRating = productList.getRating();
                float floatBasePrice = Float.parseFloat(productList.getProductprice());
                int intBasePrice = Math.round(floatBasePrice);
                float floatSalePrice = Float.parseFloat(productList.getProductsale_price());
                int intSalePrice = Math.round(floatSalePrice);
                final String product_id = productList.getProductId();
                String exists_in_wishlist = productList.getExists_in_wishlist();
                viewholder.textProductName.setText(productList.getProductname());

                final String slug = productList.getSlug();
                final String name = productList.getProductname();

                if (!strRating.equals("")) {
                    float rating = Float.parseFloat(strRating);
                    viewholder.reviewRatingbar.setVisibility(View.VISIBLE);
                    viewholder.reviewRatingbar.setRating(rating);
                } else {
                    float rating = Float.parseFloat("0");
                    viewholder.reviewRatingbar.setVisibility(View.VISIBLE);
                    viewholder.reviewRatingbar.setRating(rating);
                }

                if (intSalePrice > 0) {
                    viewholder.mTxtDiscount.setVisibility(View.VISIBLE);
                    viewholder.mTxtDiscount.setText(String.valueOf(Global.CalculateDiscount(floatSalePrice, floatBasePrice)) + " % ");
                    if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                            StaticUtility.CurrencySignPosition).equals("1")) {
                        viewholder.textProductSalePrice.setText(SharedPreference.GetPreference(context,
                                Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + " " + productList.getProductsale_price());
                        viewholder.txt_product_base_price.setText(SharedPreference.GetPreference(context,
                                Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + " " + productList.getProductprice());
                    } else if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                            StaticUtility.CurrencySignPosition).equals("2")) {
                        viewholder.textProductSalePrice.setText(productList.getProductsale_price() + ". " +
                                SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign));
                        viewholder.txt_product_base_price.setText(productList.getProductprice() + " " +
                                SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign));
                    }
                } else {
                    viewholder.mTxtDiscount.setVisibility(View.GONE);
                    if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                            StaticUtility.CurrencySignPosition).equals("1")) {
                        viewholder.textProductSalePrice.setText(SharedPreference.GetPreference(context,
                                Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + " " + productList.getProductprice());
                        viewholder.txt_product_base_price.setVisibility(View.GONE);
                    } else if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                            StaticUtility.CurrencySignPosition).equals("2")) {
                        viewholder.textProductSalePrice.setText(productList.getProductprice() + " " +
                                SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign));
                        viewholder.txt_product_base_price.setVisibility(View.GONE);
                    }
                }

                if (is_wishlist.equals("1")) {
                    viewholder.imgWish.setVisibility(View.VISIBLE);
                } else {
                    viewholder.imgWish.setVisibility(View.GONE);
                }

                if (exists_in_wishlist.equals("0")) {
                    viewholder.imgWish.setImageResource(R.drawable.ic_heart);
                } else if (exists_in_wishlist.equals("1")) {
                    viewholder.imgWish.setImageResource(R.drawable.ic_select_heart);
                }

                viewholder.imgWish.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID) != null) {
                            userID = SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID);
                            if (!userID.equalsIgnoreCase("")) {
                                AddToWishList(product_id, viewholder.imgWish, position, recyclerviewBestSelling, "bestselling");
                            } else {
                                Toast.makeText(context, "You have to Login!", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(context, LoginActivity.class);
                                SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                                SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                                SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                                SharedPreference.ClearPreference(context, Global.Billing_Preference);
                                SharedPreference.ClearPreference(context, Global.ISCheck);
                                startActivity(intent);
                                /*finish();*/
                            }
                        } else {
                            Toast.makeText(context, "You have to Login!", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(context, LoginActivity.class);
                            SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                            SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                            SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                            SharedPreference.ClearPreference(context, Global.Billing_Preference);
                            SharedPreference.ClearPreference(context, Global.ISCheck);
                            startActivity(intent);
                            /*finish();*/
                        }
                    }
                });

                viewholder.framelayoutProduct.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            String product_id = productList.getProductId();
                            ProductDetailFragment productDetailFragment = new ProductDetailFragment();
                            bundle.putString("slug", slug);
                            bundle.putString("name", name);
                            bundle.putString("ProductID", product_id);
                            movetofragment(bundle, productDetailFragment);
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }

                    }
                });

                String images = productList.getMain_image();

                //region Image
                String picUrl = null;
                try {
                    URL urla = null;
                    urla = new URL(images.replaceAll("%20", " "));
                    URI urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(), urla.getPath(), urla.getQuery(), urla.getRef());
                    picUrl = String.valueOf(urin.toURL());
                    // Capture position and set to the ImageView
                    Picasso.get()
                            .load(picUrl)
                            .into(viewholder.imageProduct, new com.squareup.picasso.Callback() {
                                @Override
                                public void onSuccess() {
                                    //holder.pbHome.setVisibility(View.INVISIBLE);
                                    viewholder.rlImgHolder.setVisibility(View.GONE);
                                }

                                @Override
                                public void onError(Exception e) {
                                    //holder.pbHome.setVisibility(View.INVISIBLE);
                                }
                            });
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    //Creating SendMail object
                    SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in MainActivity.java When parsing url\n" + e.toString());
                    //Executing sendmail to send email
                    sm.execute();
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                    //Creating SendMail object
                    SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in MainActivity.java When parsing url\n" + e.toString());
                    //Executing sendmail to send email
                    sm.execute();
                }
                //endregion

            } catch (NullPointerException e) {
                e.printStackTrace();
            }

        }

        @Override
        public int getItemCount() {
            return productLists.size();
        }

        public class Viewholder extends RecyclerView.ViewHolder {
            ImageView imageProduct, imgWish;
            TextView textProductName, textProductSalePrice, txt_product_base_price;
            FrameLayout framelayoutProduct;
            RatingBar reviewRatingbar, ratingBar;
            FrameLayout flProducts;
            RelativeLayout rlImgHolder;
            ProgressBar pbImgHolder;
            private TextView mTxtDiscount;

            public Viewholder(View itemView) {
                super(itemView);
                framelayoutProduct = (FrameLayout) itemView.findViewById(R.id.framelayoutProduct);
                imageProduct = (ImageView) itemView.findViewById(R.id.imageProduct);
                imgWish = (ImageView) itemView.findViewById(R.id.imgWish);
                textProductName = (TextView) itemView.findViewById(R.id.textProductName);
                textProductSalePrice = (TextView) itemView.findViewById(R.id.textProductSalePrice);
                txt_product_base_price = (TextView) itemView.findViewById(R.id.txt_product_base_price);
                reviewRatingbar = (RatingBar) itemView.findViewById(R.id.reviewRatingbar);
                ratingBar = (RatingBar) itemView.findViewById(R.id.ratingBar);
                flProducts = itemView.findViewById(R.id.flProducts);
                rlImgHolder = (RelativeLayout) itemView.findViewById(R.id.rlImgHolder);
                pbImgHolder = (ProgressBar) itemView.findViewById(R.id.pbImgHolder);
                mTxtDiscount = itemView.findViewById(R.id.txtDiscount);

                LayerDrawable stars = (LayerDrawable) reviewRatingbar.getProgressDrawable();
                stars.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
                stars.getDrawable(1).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);

                LayerDrawable starRating = (LayerDrawable) ratingBar.getProgressDrawable();
                starRating.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
                starRating.getDrawable(1).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);

                textProductName.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
                textProductSalePrice.setTypeface(Typefaces.TypefaceCalibri_bold(context));
                txt_product_base_price.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
                mTxtDiscount.setTypeface(Typefaces.TypefaceCalibri_bold(context));
                if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor) != "") {
                    textProductName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                    textProductSalePrice.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                }
                if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor) != "") {
                    txt_product_base_price.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
                }
            }
        }

    }
    //endregion

    //region FOR getWantedProduct API
    private void getWantedProduct() {
        if (MostWantedSetup) {
            relativeProgress.setVisibility(View.VISIBLE);
        }

        String[] key;
        String[] val;

        String user_id = SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID);
        if (SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID) != null) {
            key = new String[]{"user_id", "offset", "limit"};
            val = new String[]{user_id, String.valueOf(mMostWantedOffset),
                    String.valueOf(mMostWantedLimit)};
        } else {
            key = new String[]{"offset", "limit"};
            val = new String[]{String.valueOf(mMostWantedOffset),
                    String.valueOf(mMostWantedLimit)};
        }

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.getWantedProduct);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                try {
                    Log.d("Test", "onResponse object : " + response.toString());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            /*itemVendorStocks.clear();*/
                            relativeProgress.setVisibility(View.GONE);
                        }
                    });
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    llMostWanted.setVisibility(View.VISIBLE);
                                    JSONObject jsonObjectPayload = response.getJSONObject("payload");
                                    if (jsonObjectPayload.has("mostWantedProducts")) {
                                        String image = "";
                                        JSONArray jsonArrayWanted = jsonObjectPayload.getJSONArray("mostWantedProducts");
                                        if (jsonArrayWanted.length() > 0) {
                                            if (MostWantedSetup) {
                                                MostWantedProductLists = new ArrayList<>();
                                            }
                                            for (int i = 0; i < jsonArrayWanted.length(); i++) {
                                                JSONObject jsonObject = jsonArrayWanted.getJSONObject(i);
                                                String productName = jsonObject.getString("name");
                                                String ProductSlug = jsonObject.getString("slug");
                                                String BasePrice = jsonObject.getString("price");
                                                String SalePrice = jsonObject.getString("sale_price");
                                                String product_id = jsonObject.getString("product_id");
                                                String exists_in_wishlist = jsonObject.getString("exists_in_wishlist");
                                                String strRating = jsonObject.getString("avg_rating");
                                                Object objImageURL = jsonObject.get("main_image");
                                                if (!objImageURL.equals("")) {
                                                    JSONObject jsonObjectImages = jsonObject.getJSONObject("main_image");
                                                    image = jsonObjectImages.getString("main_image");
                                                }
                                                MostWantedProductLists.add(new ProductList(productName, BasePrice, SalePrice,
                                                        product_id, image, ProductSlug, exists_in_wishlist, strRating));
                                            }

                                            if (MostWantedSetup) {
                                                MostWantedLoading = true;
                                                mMostWantedOffset = mMostWantedOffset + mMostWantedLimit;
                                                AdapterMostWanted adapterMostWanted = new AdapterMostWanted(context, MostWantedProductLists);
                                                recyclerviewMostwanted.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                                                recyclerviewMostwanted.setAdapter(adapterMostWanted);
                                            } else {
                                                if (jsonArrayWanted.length() == mMostWantedLimit) {
                                                    mMostWantedOffset = mMostWantedOffset + mMostWantedLimit;
                                                    MostWantedLoading = true;
                                                } else {
                                                    MostWantedLoading = false;
                                                }
                                                recyclerviewMostwanted.getAdapter().notifyDataSetChanged();
                                            }
                                        } else {
                                            if (MostWantedSetup) {
                                                recyclerviewMostwanted.setVisibility(View.GONE);
                                            }
                                        }

                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in HomeFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region AdapterMostWanted
    public class AdapterMostWanted extends RecyclerView.Adapter<AdapterMostWanted.Viewholder> {

        Context context;
        JSONArray jsonArray;
        ArrayList<ProductList> productLists;


        public AdapterMostWanted(Context context, JSONArray jsonArray, String type) {
            this.context = context;
            this.jsonArray = jsonArray;
        }

        public AdapterMostWanted(Context context, ArrayList<ProductList> productLists) {
            this.context = context;
            this.productLists = productLists;
        }

        @Override
        public AdapterMostWanted.Viewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = null;
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_product, viewGroup, false);
            return new AdapterMostWanted.Viewholder(view);
        }

        @Override
        public void onBindViewHolder(final Viewholder viewholder, final int position) {
            try {
                if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        viewholder.pbImgHolder.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));
                    }
                }

                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) viewholder.flProducts.getLayoutParams();
                if (position == 0) {
                    params.setMargins(15, 0, 10, 0);
                    viewholder.flProducts.setLayoutParams(params);
                } else if (position == getItemCount() - 1) {
                    params.setMargins(10, 0, 15, 0);
                    viewholder.flProducts.setLayoutParams(params);
                } else {
                    params.setMargins(10, 0, 10, 0);
                    viewholder.flProducts.setLayoutParams(params);
                }

                final ProductList productList = productLists.get(position);
                String strRating = productList.getRating();
                float floatBasePrice = Float.parseFloat(productList.getProductprice());
                int intBasePrice = Math.round(floatBasePrice);
                float floatSalePrice = Float.parseFloat(productList.getProductsale_price());
                int intSalePrice = Math.round(floatSalePrice);
                final String product_id = productList.getProductId();
                String exists_in_wishlist = productList.getExists_in_wishlist();
                viewholder.textProductName.setText(productList.getProductname());

                final String slug = productList.getSlug();
                final String name = productList.getProductname();

                if (!strRating.equals("")) {
                    float rating = Float.parseFloat(strRating);
                    viewholder.reviewRatingbar.setVisibility(View.VISIBLE);
                    viewholder.reviewRatingbar.setRating(rating);
                } else {
                    float rating = Float.parseFloat("0");
                    viewholder.reviewRatingbar.setVisibility(View.VISIBLE);
                    viewholder.reviewRatingbar.setRating(rating);
                }

                if (intSalePrice > 0) {
                    viewholder.mTxtDiscount.setVisibility(View.VISIBLE);
                    viewholder.mTxtDiscount.setText(String.valueOf(Global.CalculateDiscount(floatSalePrice, floatBasePrice)) + " % ");
                    if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                            StaticUtility.CurrencySignPosition).equals("1")) {
                        viewholder.textProductSalePrice.setText(SharedPreference.GetPreference(context,
                                Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + " " + productList.getProductsale_price());
                        viewholder.txt_product_base_price.setText(SharedPreference.GetPreference(context,
                                Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + " " + productList.getProductprice());
                    } else if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                            StaticUtility.CurrencySignPosition).equals("2")) {
                        viewholder.textProductSalePrice.setText(productList.getProductsale_price() + ". " +
                                SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign));
                        viewholder.txt_product_base_price.setText(productList.getProductprice() + ". " +
                                SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign));
                    }
                } else {
                    viewholder.mTxtDiscount.setVisibility(View.GONE);
                    if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                            StaticUtility.CurrencySignPosition).equals("1")) {
                        viewholder.textProductSalePrice.setText(SharedPreference.GetPreference(context,
                                Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + " " + productList.getProductprice());
                        viewholder.txt_product_base_price.setVisibility(View.GONE);
                    } else if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                            StaticUtility.CurrencySignPosition).equals("2")) {
                        viewholder.textProductSalePrice.setText(productList.getProductprice() + " " +
                                SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign));
                        viewholder.txt_product_base_price.setVisibility(View.GONE);
                    }
                }

                if (is_wishlist.equals("1")) {
                    viewholder.imgWish.setVisibility(View.VISIBLE);
                } else {
                    viewholder.imgWish.setVisibility(View.GONE);
                }

                if (exists_in_wishlist.equals("0")) {
                    viewholder.imgWish.setImageResource(R.drawable.ic_heart);
                } else if (exists_in_wishlist.equals("1")) {
                    viewholder.imgWish.setImageResource(R.drawable.ic_select_heart);
                }

                viewholder.imgWish.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID) != null) {
                            userID = SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID);
                            if (!userID.equalsIgnoreCase("")) {
                                AddToWishList(product_id, viewholder.imgWish, position,
                                        recyclerviewBestSelling, "mostwanted");
                            } else {
                                Toast.makeText(context, "You have to Login!", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(context, LoginActivity.class);
                                SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                                SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                                SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                                SharedPreference.ClearPreference(context, Global.Billing_Preference);
                                SharedPreference.ClearPreference(context, Global.ISCheck);
                                startActivity(intent);
                                /*finish();*/
                            }
                        } else {
                            Toast.makeText(context, "You have to Login!", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(context, LoginActivity.class);
                            SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                            SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                            SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                            SharedPreference.ClearPreference(context, Global.Billing_Preference);
                            SharedPreference.ClearPreference(context, Global.ISCheck);
                            startActivity(intent);
                            /*finish();*/
                        }
                    }
                });

                viewholder.framelayoutProduct.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            String product_id = productList.getProductId();
                            ProductDetailFragment productDetailFragment = new ProductDetailFragment();
                            bundle.putString("slug", slug);
                            bundle.putString("name", name);
                            bundle.putString("ProductID", product_id);
                            movetofragment(bundle, productDetailFragment);
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }

                    }
                });

                String images = productList.getMain_image();

                //region Image
                String picUrl = null;
                try {
                    URL urla = null;
                    urla = new URL(images.replaceAll("%20", " "));
                    URI urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(), urla.getPath(), urla.getQuery(), urla.getRef());
                    picUrl = String.valueOf(urin.toURL());
                    // Capture position and set to the ImageView
                    Picasso.get()
                            .load(picUrl)
                            .into(viewholder.imageProduct, new com.squareup.picasso.Callback() {
                                @Override
                                public void onSuccess() {
                                    //holder.pbHome.setVisibility(View.INVISIBLE);
                                    viewholder.rlImgHolder.setVisibility(View.GONE);
                                }

                                @Override
                                public void onError(Exception e) {
                                    //holder.pbHome.setVisibility(View.INVISIBLE);
                                }
                            });
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    //Creating SendMail object
                    SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in MainActivity.java When parsing url\n" + e.toString());
                    //Executing sendmail to send email
                    sm.execute();
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                    //Creating SendMail object
                    SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in MainActivity.java When parsing url\n" + e.toString());
                    //Executing sendmail to send email
                    sm.execute();
                }
                //endregion

            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return productLists.size();
        }

        public class Viewholder extends RecyclerView.ViewHolder {
            ImageView imageProduct, imgWish;
            TextView textProductName, textProductSalePrice, txt_product_base_price;
            FrameLayout framelayoutProduct;
            RatingBar reviewRatingbar, ratingBar;
            FrameLayout flProducts;
            RelativeLayout rlImgHolder;
            ProgressBar pbImgHolder;
            private TextView mTxtDiscount;

            public Viewholder(View itemView) {
                super(itemView);
                framelayoutProduct = (FrameLayout) itemView.findViewById(R.id.framelayoutProduct);
                imageProduct = (ImageView) itemView.findViewById(R.id.imageProduct);
                imgWish = (ImageView) itemView.findViewById(R.id.imgWish);
                textProductName = (TextView) itemView.findViewById(R.id.textProductName);
                textProductSalePrice = (TextView) itemView.findViewById(R.id.textProductSalePrice);
                txt_product_base_price = (TextView) itemView.findViewById(R.id.txt_product_base_price);
                reviewRatingbar = (RatingBar) itemView.findViewById(R.id.reviewRatingbar);
                ratingBar = (RatingBar) itemView.findViewById(R.id.ratingBar);
                flProducts = itemView.findViewById(R.id.flProducts);
                rlImgHolder = (RelativeLayout) itemView.findViewById(R.id.rlImgHolder);
                pbImgHolder = (ProgressBar) itemView.findViewById(R.id.pbImgHolder);
                mTxtDiscount = itemView.findViewById(R.id.txtDiscount);

                LayerDrawable stars = (LayerDrawable) reviewRatingbar.getProgressDrawable();
                stars.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
                stars.getDrawable(1).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);

                LayerDrawable starRating = (LayerDrawable) ratingBar.getProgressDrawable();
                starRating.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
                starRating.getDrawable(1).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);

                mTxtDiscount.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
                textProductName.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
                textProductSalePrice.setTypeface(Typefaces.TypefaceCalibri_bold(context));
                txt_product_base_price.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
                if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor) != "") {
                    textProductName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                    textProductSalePrice.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                }
                if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor) != "") {
                    txt_product_base_price.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
                }
            }
        }

    }
    //endregion

    //region FOR getNewArrivals API
    private void getNewArrivals() {
        if (NewArrivalSetup) {
            relativeProgress.setVisibility(View.VISIBLE);
        }

        String[] key;
        String[] val;

        String user_id = SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID);
        if (SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID) != null) {
            key = new String[]{"user_id", "offset", "limit"};
            val = new String[]{user_id, String.valueOf(mNewArrivalOffset),
                    String.valueOf(mNewArrivalLimit)};
        } else {
            key = new String[]{"offset", "limit"};
            val = new String[]{String.valueOf(mNewArrivalOffset),
                    String.valueOf(mNewArrivalLimit)};
        }


        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.getNewArrivals);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                try {
                    Log.d("Test", "onResponse object : " + response.toString());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            /*itemVendorStocks.clear();*/
                            relativeProgress.setVisibility(View.GONE);
                        }
                    });
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    llArrivals.setVisibility(View.VISIBLE);
                                    JSONObject jsonObjectPayload = response.getJSONObject("payload");
                                    if (jsonObjectPayload.has("newArrivals")) {
                                        String image = "";
                                        JSONArray jsonArrayNewArrivals = jsonObjectPayload.getJSONArray("newArrivals");
                                        if (jsonArrayNewArrivals.length() > 0) {
                                            if (NewArrivalSetup) {
                                                NewArrivalProductLists = new ArrayList<>();
                                            }
                                            for (int i = 0; i < jsonArrayNewArrivals.length(); i++) {
                                                JSONObject jsonObject = jsonArrayNewArrivals.getJSONObject(i);
                                                String productName = jsonObject.getString("name");
                                                String ProductSlug = jsonObject.getString("slug");
                                                String BasePrice = jsonObject.getString("price");
                                                String SalePrice = jsonObject.getString("sale_price");
                                                String product_id = jsonObject.getString("product_id");
                                                String exists_in_wishlist = jsonObject.getString("exists_in_wishlist");
                                                String strRating = jsonObject.getString("avg_rating");
                                                Object objImageURL = jsonObject.get("main_image");
                                                if (!objImageURL.equals("")) {
                                                    JSONObject jsonObjectImages = jsonObject.getJSONObject("main_image");
                                                    image = jsonObjectImages.getString("main_image");
                                                }
                                                NewArrivalProductLists.add(new ProductList(productName, BasePrice, SalePrice,
                                                        product_id, image, ProductSlug, exists_in_wishlist, strRating));
                                            }
                                            if (NewArrivalSetup) {
                                                NewArrivalLoading = true;
                                                mNewArrivalOffset = mNewArrivalOffset + mNewArrivalLimit;
                                                AdapterNewArrivals adapterNewArrival = new AdapterNewArrivals(context, NewArrivalProductLists);
                                                recyclerviewNewArrivals.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                                                recyclerviewNewArrivals.setAdapter(adapterNewArrival);
                                            } else {
                                                if (jsonArrayNewArrivals.length() == mNewArrivalLimit) {
                                                    mNewArrivalOffset = mNewArrivalOffset + mNewArrivalLimit;
                                                    NewArrivalLoading = true;
                                                } else {
                                                    NewArrivalLoading = false;
                                                }
                                                recyclerviewNewArrivals.getAdapter().notifyDataSetChanged();
                                            }
                                        } else {
                                            if (NewArrivalSetup) {
                                                recyclerviewNewArrivals.setVisibility(View.GONE);
                                            }
                                        }
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in HomeFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region AdapterNewArrivals
    public class AdapterNewArrivals extends RecyclerView.Adapter<AdapterNewArrivals.Viewholder> {

        Context context;
        JSONArray jsonArray;
        ArrayList<ProductList> productLists;

        public AdapterNewArrivals(Context context, JSONArray jsonArray, String type) {
            this.context = context;
            this.jsonArray = jsonArray;
        }

        public AdapterNewArrivals(Context context, ArrayList<ProductList> productLists) {
            this.context = context;
            this.productLists = productLists;
        }

        @Override
        public AdapterNewArrivals.Viewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = null;
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_product, viewGroup, false);
            return new AdapterNewArrivals.Viewholder(view);
        }

        @Override
        public void onBindViewHolder(final Viewholder viewholder, final int position) {
            try {
                if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        viewholder.pbImgHolder.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));
                    }
                }

                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) viewholder.flProducts.getLayoutParams();
                if (position == 0) {
                    params.setMargins(15, 0, 10, 0);
                    viewholder.flProducts.setLayoutParams(params);
                } else if (position == getItemCount() - 1) {
                    params.setMargins(10, 0, 15, 0);
                    viewholder.flProducts.setLayoutParams(params);
                } else {
                    params.setMargins(10, 0, 10, 0);
                    viewholder.flProducts.setLayoutParams(params);
                }

                final ProductList productList = productLists.get(position);
                String strRating = productList.getRating();
                float floatBasePrice = Float.parseFloat(productList.getProductprice());
                int intBasePrice = Math.round(floatBasePrice);
                float floatSalePrice = Float.parseFloat(productList.getProductsale_price());
                int intSalePrice = Math.round(floatSalePrice);
                final String product_id = productList.getProductId();
                String exists_in_wishlist = productList.getExists_in_wishlist();
                viewholder.textProductName.setText(productList.getProductname());

                final String slug = productList.getSlug();
                final String name = productList.getProductname();

                if (!strRating.equals("")) {
                    float rating = Float.parseFloat(strRating);
                    viewholder.reviewRatingbar.setVisibility(View.VISIBLE);
                    viewholder.reviewRatingbar.setRating(rating);
                } else {
                    float rating = Float.parseFloat("0");
                    viewholder.reviewRatingbar.setVisibility(View.VISIBLE);
                    viewholder.reviewRatingbar.setRating(rating);
                }

                if (intSalePrice > 0) {
                    viewholder.mTxtDiscount.setVisibility(View.VISIBLE);
                    viewholder.mTxtDiscount.setText(String.valueOf(Global.CalculateDiscount(floatSalePrice, floatBasePrice)) + " % ");
                    if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                            StaticUtility.CurrencySignPosition).equals("1")) {
                        viewholder.textProductSalePrice.setText(SharedPreference.GetPreference(context,
                                Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + " " + productList.getProductsale_price());
                        viewholder.txt_product_base_price.setText(SharedPreference.GetPreference(context,
                                Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + " " + productList.getProductprice());
                    } else if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                            StaticUtility.CurrencySignPosition).equals("2")) {
                        viewholder.textProductSalePrice.setText(productList.getProductsale_price() + ". " +
                                SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign));
                        viewholder.txt_product_base_price.setText(productList.getProductprice() + " " +
                                SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign));
                    }
                } else {
                    viewholder.mTxtDiscount.setVisibility(View.GONE);
                    if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                            StaticUtility.CurrencySignPosition).equals("1")) {
                        viewholder.textProductSalePrice.setText(SharedPreference.GetPreference(context,
                                Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + " " + productList.getProductprice());
                        viewholder.txt_product_base_price.setVisibility(View.GONE);
                    } else if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                            StaticUtility.CurrencySignPosition).equals("2")) {
                        viewholder.textProductSalePrice.setText(productList.getProductprice() + " " +
                                SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign));
                        viewholder.txt_product_base_price.setVisibility(View.GONE);
                    }
                }

                if (is_wishlist.equals("1")) {
                    viewholder.imgWish.setVisibility(View.VISIBLE);
                } else {
                    viewholder.imgWish.setVisibility(View.GONE);
                }

                if (exists_in_wishlist.equals("0")) {
                    viewholder.imgWish.setImageResource(R.drawable.ic_heart);
                } else if (exists_in_wishlist.equals("1")) {
                    viewholder.imgWish.setImageResource(R.drawable.ic_select_heart);
                }

                viewholder.imgWish.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID) != null) {
                            userID = SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID);
                            if (!userID.equalsIgnoreCase("")) {
                                AddToWishList(product_id, viewholder.imgWish, position,
                                        recyclerviewBestSelling, "newarrival");
                            } else {
                                Toast.makeText(context, "You have to Login!", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(context, LoginActivity.class);
                                SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                                SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                                SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                                SharedPreference.ClearPreference(context, Global.Billing_Preference);
                                SharedPreference.ClearPreference(context, Global.ISCheck);
                                startActivity(intent);
                                /*finish();*/
                            }
                        } else {
                            Toast.makeText(context, "You have to Login!", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(context, LoginActivity.class);
                            SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                            SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                            SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                            SharedPreference.ClearPreference(context, Global.Billing_Preference);
                            SharedPreference.ClearPreference(context, Global.ISCheck);
                            startActivity(intent);
                            /*finish();*/
                        }
                    }
                });

                viewholder.framelayoutProduct.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            String product_id = productList.getProductId();
                            ProductDetailFragment productDetailFragment = new ProductDetailFragment();
                            bundle.putString("slug", slug);
                            bundle.putString("name", name);
                            bundle.putString("ProductID", product_id);
                            movetofragment(bundle, productDetailFragment);
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }

                    }
                });

                String images = productList.getMain_image();

                //region Image
                String picUrl = null;
                try {
                    URL urla = null;
                    urla = new URL(images.replaceAll("%20", " "));
                    URI urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(), urla.getPath(), urla.getQuery(), urla.getRef());
                    picUrl = String.valueOf(urin.toURL());
                    // Capture position and set to the ImageView
                    Picasso.get()
                            .load(picUrl)
                            .into(viewholder.imageProduct, new com.squareup.picasso.Callback() {
                                @Override
                                public void onSuccess() {
                                    //holder.pbHome.setVisibility(View.INVISIBLE);
                                    viewholder.rlImgHolder.setVisibility(View.GONE);
                                }

                                @Override
                                public void onError(Exception e) {
                                    //holder.pbHome.setVisibility(View.INVISIBLE);
                                }
                            });
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    //Creating SendMail object
                    SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in MainActivity.java When parsing url\n" + e.toString());
                    //Executing sendmail to send email
                    sm.execute();
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                    //Creating SendMail object
                    SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in MainActivity.java When parsing url\n" + e.toString());
                    //Executing sendmail to send email
                    sm.execute();
                }
                //endregion

            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return productLists.size();
        }

        public class Viewholder extends RecyclerView.ViewHolder {
            ImageView imageProduct, imgWish;
            TextView textProductName, textProductSalePrice, txt_product_base_price;
            FrameLayout framelayoutProduct;
            RatingBar reviewRatingbar, ratingBar;
            FrameLayout flProducts;
            RelativeLayout rlImgHolder;
            ProgressBar pbImgHolder;
            private TextView mTxtDiscount;

            public Viewholder(View itemView) {
                super(itemView);
                framelayoutProduct = (FrameLayout) itemView.findViewById(R.id.framelayoutProduct);
                imageProduct = (ImageView) itemView.findViewById(R.id.imageProduct);
                imgWish = (ImageView) itemView.findViewById(R.id.imgWish);
                textProductName = (TextView) itemView.findViewById(R.id.textProductName);
                textProductSalePrice = (TextView) itemView.findViewById(R.id.textProductSalePrice);
                txt_product_base_price = (TextView) itemView.findViewById(R.id.txt_product_base_price);
                reviewRatingbar = (RatingBar) itemView.findViewById(R.id.reviewRatingbar);
                ratingBar = (RatingBar) itemView.findViewById(R.id.ratingBar);
                flProducts = itemView.findViewById(R.id.flProducts);
                rlImgHolder = (RelativeLayout) itemView.findViewById(R.id.rlImgHolder);
                pbImgHolder = (ProgressBar) itemView.findViewById(R.id.pbImgHolder);
                mTxtDiscount = itemView.findViewById(R.id.txtDiscount);

                LayerDrawable stars = (LayerDrawable) reviewRatingbar.getProgressDrawable();
                stars.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
                stars.getDrawable(1).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);

                LayerDrawable starRating = (LayerDrawable) ratingBar.getProgressDrawable();
                starRating.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
                starRating.getDrawable(1).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);

                mTxtDiscount.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
                textProductName.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
                textProductSalePrice.setTypeface(Typefaces.TypefaceCalibri_bold(context));
                txt_product_base_price.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
                if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor) != "") {
                    textProductName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                    textProductSalePrice.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                }
                if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor) != "") {
                    txt_product_base_price.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
                }
            }
        }

    }
    //endregion

    //region FOR getBenners API
    private void getBenners() {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {};
        String[] val = {};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.getBenner);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    JSONObject jsonObjectPayload = response.getJSONObject("payload");
                                    if (jsonObjectPayload.has("images")) {
                                        JSONArray jsonArrayImages = jsonObjectPayload.getJSONArray("images");
                                        mLlBenner.setVisibility(View.VISIBLE);
                                        jsonArrayBenner = jsonArrayImages;
                                        ArrayList<Banner> banners = new ArrayList<>();
                                        for (int i = 1; i < jsonArrayBenner.length(); i++) {
                                            JSONObject jsonObject = (JSONObject) jsonArrayBenner.get(i);
                                            JSONObject jsonObjectImage = jsonObject.getJSONObject("image_url");
                                            String image = jsonObjectImage.getString("main_image");
                                            String strTitle = jsonObject.getString("title");
                                            banners.add(new Banner(strTitle, image));
                                        }
                                        AdapterBanner adapterHomeProduct = new AdapterBanner(context, banners);
                                        recyclerviewBenner.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                                        recyclerviewBenner.setAdapter(adapterHomeProduct);
                                    } else {
                                        mLlBenner.setVisibility(View.GONE);
                                    }
                                }

                            } catch (JSONException | NullPointerException e) {
                                e.printStackTrace();
                                SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in HomeFragment.java When parsing Error response.\n" + e.toString());
                                //Executing sendmail to send email
                                sm.execute();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in HomeFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region FOR getCategory API
    private void getCategory() {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {};
        String[] val = {};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.getCategory);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                        getCMSListing();
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    recyclerviewCategories.setVisibility(View.VISIBLE);
                                    JSONArray jsonArrayPayload = response.getJSONArray("payload");
                                    categories = new ArrayList<>();
                                    drawerItems.add(new DrawerItem("Home", "home", "category"));
                                    drawerItems.add(new DrawerItem("Category", "category", ""));
                                    for (int i = 0; i < jsonArrayPayload.length(); i++) {
                                        JSONObject jsonObjectPayload = jsonArrayPayload.getJSONObject(i);
                                        String cat_name = jsonObjectPayload.getString("cat_name");
                                        String slug = jsonObjectPayload.getString("slug");
                                        JSONObject jsonObjectImage = jsonObjectPayload.getJSONObject("image_url");
                                        String images = jsonObjectImage.getString("main_image");
                                        categories.add(new Category(images, cat_name, slug));
                                        drawerItems.add(new DrawerItem(cat_name, slug, "category"));
                                    }
                                    if (categories.size() > 0) {
                                        AdapterCategories adapterCategories = new AdapterCategories(context, categories);
                                        recyclerviewCategories.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                                        recyclerviewCategories.setAdapter(adapterCategories);
                                    } else {
                                        recyclerviewCategories.setVisibility(View.GONE);
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                        getCMSListing();
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in HomeFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region FOR Deals of day API
    private void getDealProduct() {
        isRefresh = true;
        if (DealSetup) {
            relativeProgress.setVisibility(View.VISIBLE);
        }

        String[] key;
        String[] val;

        String user_id = SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID);
        if (SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID) != null) {
            key = new String[]{"user_id", "offset", "limit"};
            val = new String[]{user_id, String.valueOf(mDealOffset),
                    String.valueOf(mDealLimit)};
        } else {
            key = new String[]{"offset", "limit"};
            val = new String[]{String.valueOf(mDealOffset),
                    String.valueOf(mDealLimit)};
        }


        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.GETDEALOFDAYPRODUCTS);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                try {
                    Log.d("Test", "onResponse object : " + response.toString());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            /*itemVendorStocks.clear();*/
                            relativeProgress.setVisibility(View.GONE);
                        }
                    });
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    JSONObject jsonObjectPayload = response.getJSONObject("payload");
                                    if (jsonObjectPayload.has("dealproducts")) {
                                        JSONArray jsonArrayDeals = jsonObjectPayload.getJSONArray("dealproducts");
                                        if (jsonArrayDeals.length() > 0) {
                                            if (DealSetup) {
                                                DealProductLists = new ArrayList<>();
                                            }
                                            for (int i = 0; i < jsonArrayDeals.length(); i++) {
                                                JSONObject jsonObject = jsonArrayDeals.getJSONObject(i);
                                                String productName = jsonObject.getString("name");
                                                String ProductSlug = jsonObject.getString("slug");
                                                String BasePrice = jsonObject.getString("price");
                                                String SalePrice = jsonObject.getString("sale_price");
                                                String product_id = jsonObject.getString("product_id");
                                                String exists_in_wishlist = jsonObject.getString("exists_in_wishlist");
                                                String strRating = jsonObject.getString("avg_rating");
                                                String strStartDateTime = jsonObject.getString("start_datetime");
                                                String strExpireDateTime = jsonObject.getString("expire_datetime");
                                                String strDealPrice = jsonObject.getString("dealprice");
                                                String strDealName = jsonObject.getString("dealname");
                                                String strCurrentDateTime = jsonObject.getString("currentdatetime");
                                                JSONObject jsonObjectImages = jsonObject.getJSONObject("main_image");
                                                String image = jsonObjectImages.getString("main_image");
                                                DealProductLists.add(new ProductList(productName, BasePrice, SalePrice,
                                                        product_id, image, ProductSlug, exists_in_wishlist, strRating,
                                                        strStartDateTime, strExpireDateTime, strDealPrice, strCurrentDateTime));
                                            }
                                            Global.moveWidget(mTxtDeal);

                                            if (DealSetup) {
                                                DealLoading = true;
                                                mDealOffset = mDealOffset + mDealLimit;
                                                mLlDealsOfDays.setVisibility(View.VISIBLE);
                                                AdapterDealProducts adapterDeal = new AdapterDealProducts(context, DealProductLists);
                                                mRvDealsOfDays.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                                                mRvDealsOfDays.setAdapter(adapterDeal);
                                            } else {
                                                if (jsonArrayDeals.length() == mDealLimit) {
                                                    mDealOffset = mDealOffset + mDealLimit;
                                                    DealLoading = true;
                                                } else {
                                                    DealLoading = false;
                                                }
                                                mRvDealsOfDays.getAdapter().notifyDataSetChanged();
                                            }
                                        } else {
                                            if (DealSetup) {
                                                mTxtDeal.setVisibility(View.VISIBLE);
                                                mTxtDeal.setText(getString(R.string.hot_deals_active_today));
                                                mRvDealsOfDays.setVisibility(View.GONE);
                                                mLlDealsOfDays.setVisibility(View.GONE);
                                            }
                                        }
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in HomeFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region AdapterDealProducts
    public class AdapterDealProducts extends RecyclerView.Adapter<AdapterDealProducts.Viewholder> {

        Context context;
        ArrayList<ProductList> productLists;

        public AdapterDealProducts(Context context, ArrayList<ProductList> productLists) {
            this.context = context;
            this.productLists = productLists;
        }

        @Override
        public AdapterDealProducts.Viewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = null;
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_deal_product, viewGroup, false);
            return new AdapterDealProducts.Viewholder(view);
        }

        @Override
        public void onBindViewHolder(final Viewholder viewholder, final int position) {
            try {
                if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        viewholder.pbImgHolder.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));
                    }
                }

                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) viewholder.flProducts.getLayoutParams();
                if (position == 0) {
                    params.setMargins(15, 0, 10, 0);
                    viewholder.flProducts.setLayoutParams(params);
                } else if (position == getItemCount() - 1) {
                    params.setMargins(10, 0, 15, 0);
                    viewholder.flProducts.setLayoutParams(params);
                } else {
                    params.setMargins(10, 0, 10, 0);
                    viewholder.flProducts.setLayoutParams(params);
                }

                final ProductList productList = productLists.get(position);
                String strRating = productList.getRating();
                float floatBasePrice = Float.parseFloat(productList.getProductprice());
                int intBasePrice = Math.round(floatBasePrice);
                float floatSalePrice = Float.parseFloat(productList.getProductsale_price());
                float floatDealPrice = Float.parseFloat(productList.getDealprice());
                int intSalePrice = Math.round(floatSalePrice);
                final String product_id = productList.getProductId();
                String exists_in_wishlist = productList.getExists_in_wishlist();
                viewholder.textProductName.setText(productList.getProductname());

                final String slug = productList.getSlug();
                final String name = productList.getProductname();

                if (!strRating.equals("")) {
                    float rating = Float.parseFloat(strRating);
                    viewholder.reviewRatingbar.setVisibility(View.VISIBLE);
                    viewholder.reviewRatingbar.setRating(rating);
                } else {
                    float rating = Float.parseFloat("0");
                    viewholder.reviewRatingbar.setVisibility(View.VISIBLE);
                    viewholder.reviewRatingbar.setRating(rating);
                }

                if (productList.getDealprice().equalsIgnoreCase("")) {
                    if (intSalePrice > 0) {
                        viewholder.mTxtDiscount.setVisibility(View.VISIBLE);
                        viewholder.mTxtDiscount.setText(String.valueOf(Global.CalculateDiscount(floatSalePrice, floatBasePrice)) + " % ");
                        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                                StaticUtility.CurrencySignPosition).equals("1")) {
                            viewholder.textProductSalePrice.setText(SharedPreference.GetPreference(context,
                                    Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + " " + productList.getProductsale_price());
                            viewholder.txt_product_base_price.setText(SharedPreference.GetPreference(context,
                                    Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + " " + productList.getProductprice());
                        } else if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                                StaticUtility.CurrencySignPosition).equals("2")) {
                            viewholder.textProductSalePrice.setText(productList.getProductsale_price() + ". " +
                                    SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign));
                            viewholder.txt_product_base_price.setText(productList.getProductprice() + " " +
                                    SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign));
                        }
                    } else {
                        viewholder.mTxtDiscount.setVisibility(View.GONE);
                        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                                StaticUtility.CurrencySignPosition).equals("1")) {
                            viewholder.textProductSalePrice.setText(SharedPreference.GetPreference(context,
                                    Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + " " + productList.getProductprice());
                            viewholder.txt_product_base_price.setVisibility(View.GONE);
                        } else if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                                StaticUtility.CurrencySignPosition).equals("2")) {
                            viewholder.textProductSalePrice.setText(productList.getProductprice() + " " +
                                    SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign));
                            viewholder.txt_product_base_price.setVisibility(View.GONE);
                        }
                    }
                } else {
                    viewholder.mTxtDiscount.setVisibility(View.VISIBLE);
                    viewholder.mTxtDiscount.setText(String.valueOf(Global.CalculateDiscount(floatDealPrice, floatBasePrice)) + " % ");
                    if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                            StaticUtility.CurrencySignPosition).equals("1")) {
                        viewholder.textProductSalePrice.setText(SharedPreference.GetPreference(context,
                                Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + " " + productList.getDealprice());
                        viewholder.txt_product_base_price.setText(SharedPreference.GetPreference(context,
                                Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + " " + productList.getProductprice());
                    } else if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                            StaticUtility.CurrencySignPosition).equals("2")) {
                        viewholder.textProductSalePrice.setText(productList.getDealprice() + ". " +
                                SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign));
                        viewholder.txt_product_base_price.setText(productList.getProductprice() + " " +
                                SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign));
                    }
                }

                if (is_wishlist.equals("1")) {
                    viewholder.imgWish.setVisibility(View.VISIBLE);
                } else {
                    viewholder.imgWish.setVisibility(View.GONE);
                }

                if (exists_in_wishlist.equals("0")) {
                    viewholder.imgWish.setImageResource(R.drawable.ic_heart);
                } else if (exists_in_wishlist.equals("1")) {
                    viewholder.imgWish.setImageResource(R.drawable.ic_select_heart);
                }

                if (viewholder.countDownTimer != null) {
                    viewholder.countDownTimer.cancel();
                }
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date startDate = null, endDate = null;

                try {
                    startDate = sdf.parse(productList.getCurrentdatetime());
                    endDate = sdf.parse(productList.getExpire_datetime());
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                Calendar start_calendar = Calendar.getInstance();
                start_calendar.setTime(startDate);
                Calendar end_calendar = Calendar.getInstance();
                end_calendar.setTime(endDate);
                long start_millis = start_calendar.getTimeInMillis(); //get the start time in milliseconds
                long end_millis = end_calendar.getTimeInMillis(); //get the end time in milliseconds
                long total_millis = (end_millis - start_millis); //total time in milliseconds

                viewholder.countDownTimer = new CountDownTimer(total_millis, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {
                        isRefresh = true;
                        long days = TimeUnit.MILLISECONDS.toDays(millisUntilFinished);
                        millisUntilFinished -= TimeUnit.DAYS.toMillis(days);

                        long hours = TimeUnit.MILLISECONDS.toHours(millisUntilFinished);

                        long minutes = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(
                                TimeUnit.MILLISECONDS.toHours(millisUntilFinished));

                        long seconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(
                                TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished));

                        viewholder.mTxtDays.setText(null);
                        viewholder.mTxtHours.setText(null);
                        viewholder.mTxtMin.setText(null);
                        viewholder.mTxtSec.setText(null);

                        viewholder.mTxtDays.append(twoDigitString(days));
                        viewholder.mTxtHours.append(twoDigitString(hours));
                        viewholder.mTxtMin.append(twoDigitString(minutes));
                        viewholder.mTxtSec.append(twoDigitString(seconds));

                    }

                    private String twoDigitString(long number) {
                        if (number == 0) {
                            return "00";
                        } else if (number / 10 == 0) {
                            return "0" + number;
                        }
                        return String.valueOf(number);
                    }

                    @Override
                    public void onFinish() {
                /*mTxtDays.setText("00");
                mTxtHours.setText("00");
                mTxtMin.setText("00");
                mTxtSec.setText("00");*/
                        if (isRefresh) {
                            isRefresh = false;
                            DealLoading = true;
                            DealSetup = true;
                            mDealOffset = 0;
                            getDealProduct();
                        }
                    }
                };
                viewholder.countDownTimer.start();

                /*setTimer(productList.getCurrentdatetime(), productList.getExpire_datetime(),
                        viewholder.mTxtDays, viewholder.mTxtHours, viewholder.mTxtMin,
                        viewholder.mTxtSec);*/

                /*updateTimeRemaining(productList,
                        viewholder.mTxtDays, viewholder.mTxtHours, viewholder.mTxtMin,
                        viewholder.mTxtSec);*/

                viewholder.imgWish.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID) != null) {
                            userID = SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID);
                            if (!userID.equalsIgnoreCase("")) {
                                AddToWishList(product_id, viewholder.imgWish, position,
                                        recyclerviewBestSelling, "newarrival");
                            } else {
                                Toast.makeText(context, "You have to Login!", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(context, LoginActivity.class);
                                SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                                SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                                SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                                SharedPreference.ClearPreference(context, Global.Billing_Preference);
                                SharedPreference.ClearPreference(context, Global.ISCheck);
                                startActivity(intent);
                                /*finish();*/
                            }
                        } else {
                            Toast.makeText(context, "You have to Login!", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(context, LoginActivity.class);
                            SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                            SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                            SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                            SharedPreference.ClearPreference(context, Global.Billing_Preference);
                            SharedPreference.ClearPreference(context, Global.ISCheck);
                            startActivity(intent);
                            /*finish();*/
                        }
                    }
                });

                viewholder.framelayoutProduct.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            String product_id = productList.getProductId();
                            ProductDetailFragment productDetailFragment = new ProductDetailFragment();
                            bundle.putString("slug", slug);
                            bundle.putString("name", name);
                            bundle.putString("ProductID", product_id);
                            movetofragment(bundle, productDetailFragment);
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }

                    }
                });

                String images = productList.getMain_image();

                //region Image
                String picUrl = null;
                try {
                    URL urla = null;
                    urla = new URL(images.replaceAll("%20", " "));
                    URI urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(), urla.getPath(), urla.getQuery(), urla.getRef());
                    picUrl = String.valueOf(urin.toURL());
                    // Capture position and set to the ImageView
                    Picasso.get()
                            .load(picUrl)
                            .into(viewholder.imageProduct, new com.squareup.picasso.Callback() {
                                @Override
                                public void onSuccess() {
                                    //holder.pbHome.setVisibility(View.INVISIBLE);
                                    viewholder.rlImgHolder.setVisibility(View.GONE);
                                }

                                @Override
                                public void onError(Exception e) {
                                    //holder.pbHome.setVisibility(View.INVISIBLE);
                                }
                            });
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    //Creating SendMail object
                    SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in MainActivity.java When parsing url\n" + e.toString());
                    //Executing sendmail to send email
                    sm.execute();
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                    //Creating SendMail object
                    SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in MainActivity.java When parsing url\n" + e.toString());
                    //Executing sendmail to send email
                    sm.execute();
                }
                //endregion

            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return productLists.size();
        }

        public class Viewholder extends RecyclerView.ViewHolder {
            ImageView imageProduct, imgWish;
            TextView textProductName, textProductSalePrice, txt_product_base_price,
                    mTxtDealTime, mTxtDays, mTxtDaysLable, mTxtHoursLable, mTxtHours,
                    mTxtMinLable, mTxtMin, mTxtSecLable, mTxtSec;
            FrameLayout framelayoutProduct;
            RatingBar reviewRatingbar, ratingBar;
            FrameLayout flProducts;
            LinearLayout mLlTimer;
            RelativeLayout rlImgHolder;
            ProgressBar pbImgHolder;
            private TextView mTxtDiscount;
            CountDownTimer countDownTimer;

            public Viewholder(View itemView) {
                super(itemView);
                framelayoutProduct = (FrameLayout) itemView.findViewById(R.id.framelayoutProduct);
                imageProduct = (ImageView) itemView.findViewById(R.id.imageProduct);
                imgWish = (ImageView) itemView.findViewById(R.id.imgWish);
                textProductName = (TextView) itemView.findViewById(R.id.textProductName);
                textProductSalePrice = (TextView) itemView.findViewById(R.id.textProductSalePrice);
                txt_product_base_price = (TextView) itemView.findViewById(R.id.txt_product_base_price);
                reviewRatingbar = (RatingBar) itemView.findViewById(R.id.reviewRatingbar);
                ratingBar = (RatingBar) itemView.findViewById(R.id.ratingBar);
                flProducts = itemView.findViewById(R.id.flProducts);
                rlImgHolder = (RelativeLayout) itemView.findViewById(R.id.rlImgHolder);
                pbImgHolder = (ProgressBar) itemView.findViewById(R.id.pbImgHolder);
                mTxtDealTime = (TextView) itemView.findViewById(R.id.txtDealTime);
                mLlTimer = (LinearLayout) itemView.findViewById(R.id.llTimer);
                mTxtDaysLable = (TextView) itemView.findViewById(R.id.txtDaysLable);
                mTxtDays = (TextView) itemView.findViewById(R.id.txtDays);
                mTxtHoursLable = (TextView) itemView.findViewById(R.id.txtHoursLable);
                mTxtHours = (TextView) itemView.findViewById(R.id.txtHours);
                mTxtMinLable = (TextView) itemView.findViewById(R.id.txtMinLable);
                mTxtMin = (TextView) itemView.findViewById(R.id.txtMin);
                mTxtSecLable = (TextView) itemView.findViewById(R.id.txtSecLable);
                mTxtSec = (TextView) itemView.findViewById(R.id.txtSec);
                mTxtDiscount = itemView.findViewById(R.id.txtDiscount);

                LayerDrawable stars = (LayerDrawable) reviewRatingbar.getProgressDrawable();
                stars.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
                stars.getDrawable(1).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);

                LayerDrawable starRating = (LayerDrawable) ratingBar.getProgressDrawable();
                starRating.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
                starRating.getDrawable(1).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);

                mTxtDiscount.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
                textProductName.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
                textProductSalePrice.setTypeface(Typefaces.TypefaceCalibri_bold(context));
                txt_product_base_price.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
                mTxtDealTime.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
                mTxtDaysLable.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
                mTxtDays.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
                mTxtHoursLable.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
                mTxtHours.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
                mTxtMinLable.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
                mTxtMin.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
                mTxtSecLable.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
                mTxtSec.setTypeface(Typefaces.TypefaceCalibri_Regular(context));

                mLlTimer.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                /*mTxtDealTime.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));*/
                if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor) != "") {
                    textProductName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                    textProductSalePrice.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                }
                if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor) != "") {
                    txt_product_base_price.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
                }
            }
        }
    }
    //endregion

    @Override
    public void redirection(DrawerItem drawerItem) {
        if (drawerItem.getSlug().equalsIgnoreCase("home")) {
            startActivity(new Intent(context, MainActivity.class));
            finish();
            isDashBoard = true;
        } else {
            if (drawerItem.getType().equalsIgnoreCase("info")) {
                if (drawerItem.getSlug().equalsIgnoreCase("feedback")) {
                    if (SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID) != null) {
                        userID = SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID);
                        if (!userID.equalsIgnoreCase("")) {
                            clearBackStack();
                            FeedbackFragment feedbackFragment = new FeedbackFragment();
                            movetofragment(bundle, feedbackFragment);
                        } else {
                            Toast.makeText(context, getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(context, LoginActivity.class);
                            SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                            SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                            SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                            SharedPreference.ClearPreference(context, Global.Billing_Preference);
                            SharedPreference.ClearPreference(context, Global.ISCheck);
                            startActivity(intent);
                            /*finish();*/
                        }
                    } else {
                        Toast.makeText(context, getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(context, LoginActivity.class);
                        SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                        SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                        SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                        SharedPreference.ClearPreference(context, Global.Billing_Preference);
                        SharedPreference.ClearPreference(context, Global.ISCheck);
                        startActivity(intent);
                    }
                } else if (drawerItem.getSlug().equalsIgnoreCase("contact-us")) {
                    if (SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID) != null) {
                        userID = SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID);
                        if (!userID.equalsIgnoreCase("")) {
                            clearBackStack();
                            ContactUsFragment contactUsFragment = new ContactUsFragment();
                            movetofragment(bundle, contactUsFragment);
                        } else {
                            Toast.makeText(context, "Login Required..!", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(context, LoginActivity.class);
                            SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                            SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                            SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                            SharedPreference.ClearPreference(context, Global.Billing_Preference);
                            SharedPreference.ClearPreference(context, Global.ISCheck);
                            startActivity(intent);
                            /*finish();*/
                        }
                    } else {
                        Toast.makeText(context, getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(context, LoginActivity.class);
                        SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                        SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                        SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                        SharedPreference.ClearPreference(context, Global.Billing_Preference);
                        SharedPreference.ClearPreference(context, Global.ISCheck);
                        startActivity(intent);
                    }
                } else {
                    clearBackStack();
                    CMSFragment cmsFragment = new CMSFragment();
                    bundle = new Bundle();
                    bundle.putString("slug", drawerItem.getSlug());
                    bundle.putString("title", drawerItem.getName());
                    movetofragment(bundle, cmsFragment);
                }
            } else if (drawerItem.getType().equalsIgnoreCase("category")) {
                clearBackStack();
                ProductListFragment productListFragment = new ProductListFragment();
                bundle.putString("category_slug", drawerItem.getSlug());
                bundle.putString("catName", drawerItem.getName());
                movetofragment(bundle, productListFragment);
            }
        }
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
    }

    //region FOR Logout API
    private void Logout() {
        String usertoken = "";
        if (SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERTOKEN) != null) {
            usertoken = SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERTOKEN);
        }
        String[] key = {"user_token"};
        String[] val = {usertoken};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL + StaticUtility.Logout);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers1(context));
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                                SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                                SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                                SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                                SharedPreference.ClearPreference(context, Global.Billing_Preference);
                                SharedPreference.ClearPreference(context, Global.ISCheck);
                                Intent intent = new Intent(context, LoginActivity.class);

                                startActivity(intent);
                                finish();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                if (strCode.equalsIgnoreCase("401")) {
                                    Toast.makeText(context, getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(context, Global.Billing_Preference);
                                    SharedPreference.ClearPreference(context, Global.ISCheck);
                                    Intent intent = new Intent(context, LoginActivity.class);
                                    startActivity(intent);
                                    /*finish();*/
                                } else {
                                    Toast.makeText(context, "User logged out successfully..!", Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(context, Global.Billing_Preference);
                                    SharedPreference.ClearPreference(context, Global.ISCheck);
                                    Intent intent = new Intent(context, LoginActivity.class);
                                    startActivity(intent);
                                    /*finish();*/
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in MainActivity.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region FOR GetCartCount API
    private void GetCartCount() {

        String[] key = {};
        String[] val = {};

        String userId = SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID);
        String SessionId = SharedPreference.GetPreference(context, Global.preferenceNameGuestUSer, Global.SessionId);
        if (SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID) != null) {
            key = new String[]{"session_id", "user_id"};
            val = new String[]{"", userId};
        } else if (SharedPreference.GetPreference(context, Global.preferenceNameGuestUSer, Global.SessionId) != null) {
            key = new String[]{"session_id", "user_id"};
            val = new String[]{SessionId, ""};
        }

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.CartCount);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equalsIgnoreCase("ok")) {
                                    JSONObject jsonObject = response.getJSONObject("payload");
                                    String cart_items = jsonObject.getString("cart_items");
                                    String wishlist_cnt = jsonObject.getString("wishlist_cnt");
                                    if (Integer.parseInt(cart_items) > 0) {
                                        txtCardCount.setVisibility(View.VISIBLE);
                                        txtCardCount.setText(cart_items);
                                    } else {
                                        txtCardCount.setText(cart_items);
                                        txtCardCount.setVisibility(View.GONE);
                                    }
                                    if (Integer.parseInt(wishlist_cnt) > 0) {
                                        txtWishlistCount.setVisibility(View.VISIBLE);
                                        txtWishlistCount.setText(wishlist_cnt);
                                    } else {
                                        txtWishlistCount.setVisibility(View.GONE);
                                    }

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in MainActivity.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region AdapterCategories
    public class AdapterCategories extends RecyclerView.Adapter<AdapterCategories.Viewholder> {

        Context context;
        ArrayList<Category> categories = new ArrayList<>();

        public AdapterCategories(Context context, ArrayList<Category> categories) {
            this.context = context;
            this.categories = categories;
        }

        @Override
        public AdapterCategories.Viewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = null;
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_home_category, viewGroup, false);
            return new AdapterCategories.Viewholder(view);
        }

        @Override
        public void onBindViewHolder(final Viewholder viewholder, int position) {
            if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    viewholder.pbImgHolder.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));
                }
            }
            final Category category = categories.get(position);
            viewholder.txtCategory.setText(category.getCategory_Name());
            String images = category.getCategory_image();

            //region Image
            String picUrl = null;
            try {
                URL urla = null;
                urla = new URL(images.replaceAll("%20", " "));
                URI urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(), urla.getPath(), urla.getQuery(), urla.getRef());
                picUrl = String.valueOf(urin.toURL());
                // Capture position and set to the ImageView
                Picasso.get()
                        .load(picUrl)
                        .into(viewholder.img_Category, new com.squareup.picasso.Callback() {
                            @Override
                            public void onSuccess() {
                                //holder.pbHome.setVisibility(View.INVISIBLE);
                                viewholder.rlImgHolder.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError(Exception e) {
                                //holder.pbHome.setVisibility(View.INVISIBLE);
                            }
                        });
            } catch (MalformedURLException e) {
                e.printStackTrace();
                //Creating SendMail object
                SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in MainActivity.java When parsing url\n" + e.toString());
                //Executing sendmail to send email
                sm.execute();
            } catch (URISyntaxException e) {
                e.printStackTrace();
                //Creating SendMail object
                SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in MainActivity.java When parsing url\n" + e.toString());
                //Executing sendmail to send email
                sm.execute();
            }
            //endregion

            if (position == categories.size() - 1) {
                viewholder.viewCategory.setVisibility(View.GONE);
            }

            viewholder.llCategory.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String slug = category.getSlug();
                    String cat_name = category.getCategory_Name();
                    ProductListFragment productListFragment = new ProductListFragment();
                    bundle.putString("category_slug", slug);
                    bundle.putString("catName", cat_name);
                    movetofragment(bundle, productListFragment);
                    /*Intent intentProductList = new Intent(context, ProductListingActivity.class);
                    intentProductList.putExtra("category_slug", slug);
                    intentProductList.putExtra("catName", cat_name);
                    startActivity(intentProductList);*/
                }
            });
        }

        @Override
        public int getItemCount() {
            return categories.size();
        }

        public class Viewholder extends RecyclerView.ViewHolder {
            ImageView img_Category;
            TextView txtCategory;
            LinearLayout llCategory;
            View viewCategory;
            RelativeLayout rlImgHolder;
            ProgressBar pbImgHolder;

            public Viewholder(View itemView) {
                super(itemView);
                img_Category = (ImageView) itemView.findViewById(R.id.img_Category);
                txtCategory = (TextView) itemView.findViewById(R.id.txtCategory);
                llCategory = (LinearLayout) itemView.findViewById(R.id.llCategory);
                viewCategory = (View) itemView.findViewById(R.id.viewCategory);
                rlImgHolder = (RelativeLayout) itemView.findViewById(R.id.rlImgHolder);
                pbImgHolder = (ProgressBar) itemView.findViewById(R.id.pbImgHolder);

                txtCategory.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
                txtCategory.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));

            }
        }

    }
    //endregion

    @Override
    protected void onResume() {
        super.onResume();

        GetCartCount();
        SetUserInfo();

        if (SharedPreference.GetPreference(this, Global.LOGIN_PREFERENCE, Global.USERID) != null) {
            llLogout.setVisibility(View.VISIBLE);
            llLogIn.setVisibility(View.GONE);
        } else {
            llLogout.setVisibility(View.GONE);
            llLogIn.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void gotoChangeAddressCheckout(String AddressType, String AddressTypeName,
                                          String MethodCallFrom) {
        /*bundle = new Bundle();
        AddressListFragment addressListFragment = new AddressListFragment();
        bundle.putString("ActivityType", "MyAddressActivity");
        bundle.putString("AddressType", AddressType);
        bundle.putString("AddressTypeName", AddressTypeName);
        movetofragment(bundle, addressListFragment);*/
        getAddressList(AddressType, "CheckoutFragment", AddressTypeName, MethodCallFrom);
    }

    @Override
    public void gotoChangeAddressCheckout1(String AddressType, String AddressTypeName,
                                           String MethodCallFrom, String strShippingUserName,
                                           String strShippingAddress, String strShippingPhoneNo) {
        strShippinguserName = strShippingUserName;
        strShippingaddress = strShippingAddress;
        strShippingphoneno = strShippingPhoneNo;
        getAddressList(AddressType, "CheckoutFragment", AddressTypeName, MethodCallFrom);
    }


    @Override
    public void gotoSubmitFeedback(String product_id, String orderitem_id) {
        /*bundle = new Bundle();
        bundle.putString("orderitem_id", orderitem_id);
        bundle.putString("product_id", product_id);
        SubmitFeedbackFragment submitFeedbackFragment = new SubmitFeedbackFragment();
        movetofragment(bundle, submitFeedbackFragment);*/

        Intent intentSubmitFeedback = new Intent(context, SubmitFeedbackActivity.class);
        intentSubmitFeedback.putExtra("orderitem_id", orderitem_id);
        intentSubmitFeedback.putExtra("product_id", product_id);
        startActivity(intentSubmitFeedback);
    }

    @Override
    public void gotoCheckoutCart() {
        CheckoutFragment checkoutFragment = new CheckoutFragment();
        movetofragment(bundle, checkoutFragment);
    }

    @Override
    public void gotoReviews() {
        bundle = new Bundle();
       /* ReviewsFragment reviewsFragment = new ReviewsFragment();
        movetofragment(bundle, reviewsFragment);*/
        Intent intentReviews = new Intent(context, ReviewsActivity.class);
        startActivity(intentReviews);
    }

    @Override
    public void gotoOrderHistory() {
        /*OrderHistoryFragment orderHistoryFragment = new OrderHistoryFragment();
        movetofragment(bundle, orderHistoryFragment);*/
        Intent intentOrderHistory = new Intent(context, OrderHistoryActivity.class);
        intentOrderHistory.putExtra("Redirect", "MyAccount");
        startActivity(intentOrderHistory);
    }

    @Override
    public void gotoUserProfile() {
        /*ProfileFragment profileFragment = new ProfileFragment();
        movetofragment(bundle, profileFragment);*/
        Intent intentProfile = new Intent(context, ProfileActivity.class);
        startActivity(intentProfile);
    }

    @Override
    public void gotoChangepwd() {
        /*ChangepasswordFragment changepasswordFragment = new ChangepasswordFragment();
        movetofragment(bundle, changepasswordFragment);*/
        Intent intentChangepassword = new Intent(context, ChangepasswordActivity.class);
        startActivity(intentChangepassword);
    }

    @Override
    public void gotoMyAccountAddressListing() {
        getAddressList("", "MyAccountAddressListingActivity", "", "");
    }

    @Override
    public void gotoOrderSuccess(String payment_method, String urlStatus) {
        bundle = new Bundle();
        PlaceOrderSuccessFragment placeOrderSuccessFragment = new PlaceOrderSuccessFragment();
        bundle.putString("payment_method", payment_method);
        bundle.putString("urlStatus", urlStatus);
        movetofragment(bundle, placeOrderSuccessFragment);
    }

    @Override
    public void gotoPayment(String orderid, String amount, String Fanme, String phoneNo, String mMerchantKey, String mSalt, String Payumoneysuccessurl, String payumoneyfailureurl) {
        bundle = new Bundle();
        PaymentFragment paymentFragment = new PaymentFragment();
        bundle.putString("order_id", orderid);
        bundle.putString("amount", amount);
        bundle.putString("firstname", Fanme);
        bundle.putString("phone", phoneNo);
        bundle.putString("mMerchantKey", mMerchantKey);
        bundle.putString("mSalt", mSalt);
        bundle.putString("Payumoneysuccessurl", Payumoneysuccessurl);
        bundle.putString("payumoneyfailureurl", payumoneyfailureurl);
        movetofragment(bundle, paymentFragment);
    }

    @Override
    public void gotoCCAvence(Bundle bundle) {
        CCAvenueFragment ccAvenueFragment = new CCAvenueFragment();
        movetofragment(bundle, ccAvenueFragment);
    }

    @Override
    public void gotoProductListing(String category_slug, String CatName) {
        ProductListFragment productListFragment = new ProductListFragment();
        bundle.putString("category_slug", category_slug);
        bundle.putString("catName", CatName);
        movetofragment(bundle, productListFragment);
    }

    @Override
    public void GotoOrderDetail(String slug) {
        bundle = new Bundle();
        OrderDetailFragment orderDetailFragment = new OrderDetailFragment();
        bundle.putString("Order_Id", slug);
        movetofragment(bundle, orderDetailFragment);
    }

    @Override
    public void onAddToCartProduct() {
        qty = txtCardCount.getText().toString();
        if (qty.equals("")) {
            qty = "0";
        }
        qty = String.valueOf(Integer.parseInt(qty) + 1);
        txtCardCount.setVisibility(View.VISIBLE);
        txtCardCount.setText(qty);
    }

    @Override
    public void FilterClick(Boolean values) {
        if (values) {
            cardviewBottomNavigation.setVisibility(View.GONE);
            toolbar.setVisibility(View.GONE);
        } else {
            cardviewBottomNavigation.setVisibility(View.VISIBLE);
            toolbar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void OnProductClick(String slug, String Productname, String product_id) {
        ProductDetailFragment productDetailFragment = new ProductDetailFragment();
        bundle.putString("slug", slug);
        bundle.putString("name", Productname);
        bundle.putString("ProductID", product_id);
        movetofragment(bundle, productDetailFragment);
    }

    @Override
    public void GoToCartList() {
        CartFragment cartFragment = new CartFragment();
        movetofragment(bundle, cartFragment);
    }

    @Override
    public void WishlistCount() {
        wishlistcount = txtWishlistCount.getText().toString();
        if (wishlistcount.equals("")) {
            wishlistcount = "0";
        }
        wishlistcount = String.valueOf(Integer.parseInt(wishlistcount) + 1);
        txtWishlistCount.setVisibility(View.VISIBLE);
        txtWishlistCount.setText(wishlistcount);
        SharedPreference.CreatePreference(context, Global.WishlistCountPreference);
        SharedPreference.SavePreference(StaticUtility.WishlistCount, wishlistcount);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (s.length() >= 1) {
            getSearch(String.valueOf(s));
        } else {
            Global.HideSystemKeyboard(this);
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    public void wishlistcount() {
        wishlistcount = txtWishlistCount.getText().toString();
        if (wishlistcount.equals("")) {
            wishlistcount = "0";
        }
        wishlistcount = String.valueOf(Integer.parseInt(wishlistcount) + 1);
        txtWishlistCount.setVisibility(View.VISIBLE);
        txtWishlistCount.setText(wishlistcount);
        SharedPreference.CreatePreference(context, Global.WishlistCountPreference);
        SharedPreference.SavePreference(StaticUtility.WishlistCount, wishlistcount);
    }

    @Override
    public void wishlistcountRemove() {
        wishlistcount = txtWishlistCount.getText().toString();
        wishlistcount = String.valueOf(Integer.parseInt(wishlistcount) - 1);
        if (!wishlistcount.equalsIgnoreCase("0")) {
            txtWishlistCount.setVisibility(View.VISIBLE);
            txtWishlistCount.setText(wishlistcount);
        } else {
            txtWishlistCount.setVisibility(View.GONE);
            txtWishlistCount.setText("");
            /*SharedPreference.CreatePreference(context, Global.WishlistCountPreference);
            SharedPreference.SavePreference(StaticUtility.WishlistCount, wishlistcount);*/
        }
    }

    //region FOR AddToWishList API
    private void AddToWishList(final String product_id, final ImageView imageView,
                               final int position, final RecyclerView recyclerView, final String strType) {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {"product_id"};
        String[] val = {product_id};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL + StaticUtility.AddToWishlist);
        if (SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERTOKEN) != null) {
            postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                    .addHeaders(Global.headers1(context));
        } else {
            postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                    .addHeaders(Global.headers2(context));
        }
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                imageView.setImageResource(R.drawable.ic_select_heart);
                                Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                                wishlistcount();
                                if (strType.equalsIgnoreCase("bestselling")) {
                                    bestSellingProductLists.get(position).setExists_in_wishlist("1");
                                    recyclerView.getAdapter().notifyDataSetChanged();
                                    NewArrivalListingRefresh(product_id, "1");
                                    MostWantedListingRefresh(product_id, "1");
                                } else if (strType.equalsIgnoreCase("mostwanted")) {
                                    MostWantedProductLists.get(position).setExists_in_wishlist("1");
                                    recyclerView.getAdapter().notifyDataSetChanged();
                                    BestSellingListingRefresh(product_id, "1");
                                    NewArrivalListingRefresh(product_id, "1");
                                } else if (strType.equalsIgnoreCase("newarrival")) {
                                    NewArrivalProductLists.get(position).setExists_in_wishlist("1");
                                    recyclerView.getAdapter().notifyDataSetChanged();
                                    BestSellingListingRefresh(product_id, "1");
                                    MostWantedListingRefresh(product_id, "1");
                                }
                                /*recyclerView.getAdapter().notifyItemChanged(position);*/

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strCode.equalsIgnoreCase("401")) {
                                    Toast.makeText(context, getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(context, Global.Billing_Preference);
                                    Intent intent = new Intent(context, LoginActivity.class);
                                    startActivity(intent);
                                    /*finish();*/
                                } else if (strMessage.equals("Product already exists in Wishlist!")) {
                                    Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                                    imageView.setClickable(false);
                                } else {
                                    imageView.setClickable(true);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in MainActivity.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region For EventBus onEvent
    public void onEvent(String event) {
        openInternetAlertDialog(context, event);
    }
    //endregion

    //region FOR SHOW INTERNET CONNECTION DIALOG
    public void openInternetAlertDialog(final Context mContext, String alertString) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
        final View row = inflater.inflate(R.layout.row_alert_dialog, null);
        final TextView tvAlertText = (TextView) row.findViewById(R.id.tvAlertText);
        final TextView tvTitle = (TextView) row.findViewById(R.id.tvTitle);
        final Button btnSettings = (Button) row.findViewById(R.id.btnSettings);
        final Button btnExit = (Button) row.findViewById(R.id.btnExit);

        tvTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        tvAlertText.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        btnSettings.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        btnExit.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));

        btnExit.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        btnSettings.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        tvTitle.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));

        tvAlertText.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        tvTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        btnSettings.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        btnExit.setTypeface(Typefaces.TypefaceCalibri_Regular(context));

        try {
            if (alertString.equals("Not connected to Internet")) {
                if (i == 0) {
                    i = 1;
                    AlertDialog.Builder i_builder = new AlertDialog.Builder(mContext);
                    internetAlert = i_builder.create();
                    internetAlert.setCancelable(false);
                    internetAlert.setView(row);

                    if (internetAlert.isShowing()) {
                        internetAlert.dismiss();
                    } else {
                        internetAlert.show();
                    }

                    btnExit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            internetAlert.dismiss();
                            //FOR CLOSE APP...
                            System.exit(0);
                        }
                    });

                    btnSettings.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            /*internetAlert.dismiss();*/
                            startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                        }
                    });
                } else {
                    /*internetAlert.dismiss();*/
                }
            } else {
                i = 0;
                internetAlert.dismiss();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }
    //endregion

    //region ON ACTIVITY RESULT FOR DISMISS OR SHOW INTERNET ALERT DIALOG
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (!Global.isNetworkAvailable(context)) {
                openInternetAlertDialog(context, "Not connected to Internet");
            } else {
                internetAlert.dismiss();
            }
        }
    }
    //endregion

    //region Refresh
    public void API() {
        if (is_bestselller.equals("1")) {
            getBestSeller();
        } else {
            llBestSeller.setVisibility(View.GONE);
        }
        if (is_mostwanted.equals("1")) {
            getWantedProduct();
        } else {
            llMostWanted.setVisibility(View.GONE);
        }
        if (is_arrivals.equals("1")) {
            getNewArrivals();
        } else {
            llArrivals.setVisibility(View.GONE);
        }
    }
    //endregion

    //region FOR getCMSListing API
    private void getCMSListing() {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {};
        String[] val = {};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.GetCMSListing);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                        mLlFooter.setVisibility(View.VISIBLE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                drawerItems.add(new DrawerItem("Informatation", "info", ""));
                                if (strStatus.equals("ok")) {
                                    /*ArrayList<CMSPages> cmsPages = new ArrayList<>();*/
                                    JSONArray jsonArray = response.getJSONArray("payload");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject = jsonArray.optJSONObject(i);
                                        String strTitle = jsonObject.optString("title");
                                        String strSlug = jsonObject.optString("slug");
                                        /*drawerItems.add(new CMSPages(strTitle, strSlug));*/
                                        drawerItems.add(new DrawerItem(strTitle, strSlug, "info"));
                                    }
                                    drawerItems.add(new DrawerItem("Feedback", "feedback", "info"));
                                    drawerItems.add(new DrawerItem("Contact Us", "contact-us", "info"));

                                    if (drawerItems.size() > 0) {
                                        for (int i = 0; i < drawerItems.size(); i++) {
                                            if (drawerItems.get(i).getSlug().equalsIgnoreCase("home")) {
                                                drawerItems.get(i).setSelect(true);
                                            } else {
                                                drawerItems.get(i).setSelect(false);
                                            }
                                        }
                                        mRvDrawer.setVisibility(View.VISIBLE);
                                        DrawerItemsAdapter drawerItemsAdapter = new DrawerItemsAdapter(context, drawerItems);
                                        mRvDrawer.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
                                        mRvDrawer.setAdapter(drawerItemsAdapter);
                                    } else {
                                        mRvDrawer.setVisibility(View.GONE);

                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                        mLlFooter.setVisibility(View.VISIBLE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in MainActivity.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region ADAPTER
    public class AdapterCMSList extends RecyclerView.Adapter<AdapterCMSList.Viewholder> {

        Context context;
        ArrayList<CMSPages> cmsPages = new ArrayList<>();
        int selectedPosition = -1;

        public AdapterCMSList(Context context, ArrayList<CMSPages> cmsPages) {
            this.context = context;
            this.cmsPages = cmsPages;
        }

        @Override
        public AdapterCMSList.Viewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = null;
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_cms_list, viewGroup, false);
            return new AdapterCMSList.Viewholder(view);
        }

        @Override
        public void onBindViewHolder(final Viewholder viewholder, final int position) {
            final CMSPages cmsPage = cmsPages.get(position);
            viewholder.txtCMS.setText(cmsPage.getTitle());

            /*if (category.isSelect()) {
                viewHolder.txtCMS.setTextSize(mContext.getResources().getDimension(R.dimen.txtmenu_select_textSize) / mContext.getResources().getDisplayMetrics().density);
                viewHolder.llSelection.setVisibility(View.VISIBLE);
                viewHolder.llCMS.setBackgroundColor(mContext.getResources().getColor(R.color.selectionColor));
                viewHolder.llSelection.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                viewHolder.txtCMS.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                viewHolder.imgCMS.setColorFilter(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
            } else {
                viewHolder.llCMS.setBackgroundColor(mContext.getResources().getColor(R.color.colorcard));
                viewHolder.txtCMS.setTextSize(mContext.getResources().getDimension(R.dimen.txtmenu_textSize) / mContext.getResources().getDisplayMetrics().density);
                viewHolder.llSelection.setVisibility(View.GONE);
                viewHolder.txtCMS.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                viewHolder.imgCMS.setColorFilter(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
            }*/

            viewholder.llCMS.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (cmsPage.getSlug().equalsIgnoreCase("feedback")) {
                        if (SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID) != null) {
                            userID = SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID);
                            if (!userID.equalsIgnoreCase("")) {
                                clearBackStack();
                                FeedbackFragment feedbackFragment = new FeedbackFragment();
                                movetofragment(bundle, feedbackFragment);
                            } else {
                                /*Toast.makeText(context, "You have to Login!", Toast.LENGTH_SHORT).show();*/
                                Intent intent = new Intent(context, LoginActivity.class);
                                SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                                SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                                SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                                SharedPreference.ClearPreference(context, Global.Billing_Preference);
                                SharedPreference.ClearPreference(context, Global.ISCheck);
                                startActivity(intent);
                                /*finish();*/
                            }
                        } else {
                            /*Toast.makeText(context, "You have to Login!", Toast.LENGTH_SHORT).show();*/
                            Intent intent = new Intent(context, LoginActivity.class);
                            SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                            SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                            SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                            SharedPreference.ClearPreference(context, Global.Billing_Preference);
                            SharedPreference.ClearPreference(context, Global.ISCheck);
                            startActivity(intent);
                        }
                    } else if (cmsPage.getSlug().equalsIgnoreCase("contact-us")) {
                        if (SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID) != null) {
                            userID = SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID);
                            if (!userID.equalsIgnoreCase("")) {
                                clearBackStack();
                                ContactUsFragment contactUsFragment = new ContactUsFragment();
                                movetofragment(bundle, contactUsFragment);
                            } else {
                                /*Toast.makeText(context, "You have to Login!", Toast.LENGTH_SHORT).show();*/
                                Intent intent = new Intent(context, LoginActivity.class);
                                SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                                SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                                SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                                SharedPreference.ClearPreference(context, Global.Billing_Preference);
                                SharedPreference.ClearPreference(context, Global.ISCheck);
                                startActivity(intent);
                                /*finish();*/
                            }
                        } else {
                            /*Toast.makeText(context, "You have to Login!", Toast.LENGTH_SHORT).show();*/
                            Intent intent = new Intent(context, LoginActivity.class);
                            SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                            SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                            SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                            SharedPreference.ClearPreference(context, Global.Billing_Preference);
                            SharedPreference.ClearPreference(context, Global.ISCheck);
                            startActivity(intent);
                        }
                    } else {
                        clearBackStack();
                        CMSFragment cmsFragment = new CMSFragment();
                        bundle = new Bundle();
                        bundle.putString("slug", cmsPage.getSlug());
                        bundle.putString("title", cmsPage.getTitle());
                        movetofragment(bundle, cmsFragment);
                    }
                    if (drawer.isDrawerOpen(GravityCompat.START)) {
                        drawer.closeDrawer(GravityCompat.START);
                    }
                }
            });

        }

        @Override
        public int getItemCount() {
            /*return jsonArray.length();*/
            return cmsPages.size();
        }

        public class Viewholder extends RecyclerView.ViewHolder {
            TextView txtCMS;
            LinearLayout llCMS;

            public Viewholder(View itemView) {
                super(itemView);
                llCMS = (LinearLayout) itemView.findViewById(R.id.llCMS);
                txtCMS = (TextView) itemView.findViewById(R.id.txtCMS);

                txtCMS.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
                txtCMS.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));

            }
        }

    }
    //endregion

    //region FOR getSearch API
    private void getSearch(String search) {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {"productname", "offset", "limit"};
        String[] val = {search, "0", "20"};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.GetSearchResult);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                productItems.clear();
                                if (strStatus.equals("ok")) {
                                    JSONObject jsonObject = response.getJSONObject("payload");
                                    JSONArray jsonArray = jsonObject.getJSONArray("products");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObjectProduct = jsonArray.getJSONObject(i);
                                        String slug = jsonObjectProduct.getString("slug");
                                        String product_id = jsonObjectProduct.getString("product_id");
                                        String name = jsonObjectProduct.getString("name");

                                        productItem = new ProductItem();
                                        productItem.setProductName(name);
                                        productItem.setProductId(product_id);
                                        productItem.setSlug(slug);
                                        productItems.add(productItem);
                                    }
                                    AutoCompleteAdapter autoCompleteAdapterCity = new AutoCompleteAdapter(context, R.layout.row_product_items, productItems, editSearch);
                                    editSearch.setAdapter(autoCompleteAdapterCity);
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in MainActivity.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region AUTO COMPLETE ADAPTER
    public class AutoCompleteAdapter extends ArrayAdapter<ProductItem> {
        Context mContext;
        ArrayList<ProductItem> mDepartments;
        ArrayList<ProductItem> mDepartments_All;
        ArrayList<ProductItem> mDepartments_Suggestion;
        int mLayoutResourceId;
        private AutoCompleteTextView autoCompleteTextView;

        public AutoCompleteAdapter(Context context, int resource, ArrayList<ProductItem> departments, AutoCompleteTextView autoCompleteTextView) {
            super(context, resource, departments);
            this.mContext = context;
            this.mLayoutResourceId = resource;
            this.mDepartments = new ArrayList<>(departments);
            this.mDepartments_All = new ArrayList<>(departments);
            this.mDepartments_Suggestion = new ArrayList<>();
            this.autoCompleteTextView = autoCompleteTextView;
        }

        public int getCount() {
            return mDepartments.size();
        }

        public ProductItem getItem(int position) {
            return mDepartments.get(position);
        }

        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            try {
                if (convertView == null) {
                    LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
                    convertView = inflater.inflate(mLayoutResourceId, parent, false);
                }
                final ProductItem department = getItem(position);
                TextView name = (TextView) convertView.findViewById(R.id.productName);
                TextView id = (TextView) convertView.findViewById(R.id.productId);
                name.setText(department.getProductName());
                name.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                id.setText(department.getProductId());

                autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        ProductItem pi = getItem(position);
                        String strProductName = pi.getProductName();
                        String strProductId = pi.getProductId();
                        String strProductSlug = pi.getSlug();
                        hideKeyboard(MainActivity.this);
                        ProductDetailFragment productDetailFragment = new ProductDetailFragment();
                        bundle = new Bundle();
                        bundle.putString("slug", strProductSlug);
                        bundle.putString("name", strProductName);
                        bundle.putString("ProductID", strProductId);
                        movetofragment(bundle, productDetailFragment);
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return convertView;
        }

        @Override
        public Filter getFilter() {
            return new Filter() {
                @Override
                public String convertResultToString(Object resultValue) {
                    return ((ProductItem) resultValue).getProductName();
                }

                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    if (constraint != null) {
                        mDepartments_Suggestion.clear();
                        for (ProductItem department : mDepartments_All) {
                            if (department.getProductName().toLowerCase().startsWith(constraint.toString().toLowerCase())) {
                                mDepartments_Suggestion.add(department);
                            }
                        }
                        FilterResults filterResults = new FilterResults();
                        filterResults.values = mDepartments_Suggestion;
                        filterResults.count = mDepartments_Suggestion.size();
                        return filterResults;
                    } else {
                        return new FilterResults();
                    }
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    mDepartments.clear();
                    if (results != null && results.count > 0) {
                        // avoids unchecked cast warning when using mDepartments.addAll((ArrayList<Department>) results.values);
                        List<?> result = (List<?>) results.values;
                        for (Object object : result) {
                            if (object instanceof ProductItem) {
                                mDepartments.add((ProductItem) object);
                            }
                        }
                    } else if (constraint == null) {
                        // no filter, add entire original list back in
                        mDepartments.addAll(mDepartments_All);
                    }
                    notifyDataSetChanged();
                }
            };
        }
    }
    //endregion

    //region Keyboard Hide
    public static void hideKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
    //endregion

    //region FOR getAddressList API...
    public void getAddressList(final String type, final String ActivityType,
                               final String AddressTypeName, final String strMethodCall) {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = new String[0];
        String[] val = new String[0];

        if (ActivityType.equalsIgnoreCase("CheckoutFragment")) {
            key = new String[]{"address_type"};
            val = new String[]{type};
        } else if (ActivityType.equalsIgnoreCase("MyAccountAddressListingActivity")) {
            key = new String[]{};
            val = new String[]{};
        }

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.AddressList);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers1(context));
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    JSONArray jsonArrayPayload = response.getJSONArray("payload");
                                    if (jsonArrayPayload.length() > 0) {
                                        addressLists = new ArrayList<>();
                                        for (int i = 0; i < jsonArrayPayload.length(); i++) {
                                            JSONObject jsonObjectPayload = jsonArrayPayload.getJSONObject(i);
                                            String Fname = jsonObjectPayload.getString("first_name");
                                            String Lname = jsonObjectPayload.getString("last_name");
                                            String address = jsonObjectPayload.getString("address");
                                            String pincode = jsonObjectPayload.getString("pincode");
                                            String city = jsonObjectPayload.getString("city");
                                            String state = jsonObjectPayload.getString("state");
                                            String phone_number = jsonObjectPayload.getString("phone_number");
                                            String useradress_id = jsonObjectPayload.getString("useradress_id");
                                            String landmark = jsonObjectPayload.getString("landmark");
                                            String country = jsonObjectPayload.getString("country");
                                            String address_type = jsonObjectPayload.getString("address_type");
                                            addressLists.add(new AddressList(Fname, Lname, address, city, pincode, state, phone_number, useradress_id, landmark, country, address_type));
                                        }
                                        if (ActivityType.equalsIgnoreCase("CheckoutFragment")) {
                                            Intent intent = new Intent(context, CheckoutAddressListActivity.class);
                                            bundle.putString("ActivityType", ActivityType);
                                            bundle.putString("AddressType", type);
                                            bundle.putString("AddressTypeName", AddressTypeName);
                                            bundle.putSerializable("AddressList", addressLists);
                                            intent.putExtra("BundleAddress", bundle);
                                            startActivity(intent);
                                        } else if (ActivityType.equalsIgnoreCase("MyAccountAddressListingActivity")) {
                                            Intent intent = new Intent(context, MyAccountAddressListActivity.class);
                                            bundle.putString("ActivityType", ActivityType);
                                            bundle.putString("AddressType", type);
                                            bundle.putString("AddressTypeName", AddressTypeName);
                                            bundle.putSerializable("AddressList", addressLists);
                                            intent.putExtra("BundleAddress", bundle);
                                            startActivity(intent);
                                            /*AddressListFragment addressListFragment = new AddressListFragment();
                                            bundle.putString("ActivityType", ActivityType);
                                            bundle.putString("AddressType", type);
                                            bundle.putString("AddressTypeName", AddressTypeName);
                                            bundle.putSerializable("AddressList", addressLists);
                                            movetofragment(bundle, addressListFragment);*/
                                        }
                                    } else {
                                        if (ActivityType.equalsIgnoreCase("CheckoutFragment")) {
                                            if (strMethodCall.equalsIgnoreCase("LastAddress")) {
                                                Intent intent = new Intent(context, CheckoutAddAddressActivity.class);
                                                intent.putExtra("ActivityType", ActivityType);
                                                intent.putExtra("AddressType", "Both");
                                                intent.putExtra("Type", "Checkout");
                                                startActivity(intent);
                                            } else if (strMethodCall.equalsIgnoreCase("ChangeAddress")) {
                                                Intent intent = new Intent(context, CheckoutAddAddressActivity.class);
                                                intent.putExtra("ActivityType", ActivityType);
                                                intent.putExtra("AddressType", AddressTypeName);
                                                intent.putExtra("Type", "Checkout");
                                                intent.putExtra("ShippingUserName", strShippinguserName);
                                                intent.putExtra("ShippingAddress", strShippingaddress);
                                                intent.putExtra("ShippingPhoneNo", strShippingphoneno);
                                                startActivity(intent);
                                            }
                                        } else {
                                            Intent intent = new Intent(context, ADDAddressesActivity.class);
                                            intent.putExtra("ActivityType", ActivityType);
                                            intent.putExtra("Type", "MainActivity");
                                            startActivity(intent);
                                        }
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                if (strCode.equalsIgnoreCase("401")) {
                                    Toast.makeText(context, getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(context, Global.Billing_Preference);
                                    Intent intent = new Intent(context, LoginActivity.class);
                                    intent.putExtra("Redirect", "myaccount");
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    /*finish();*/
                                } else if (strMessage.equals("app-id and app-secret is required")) {
                                    Toast.makeText(context, getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(context, Global.Billing_Preference);
                                    Intent intent = new Intent(context, LoginActivity.class);
                                    intent.putExtra("Redirect", "myaccount");
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    /*finish();*/
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in HomeFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region SetUserInfo
    public static void SetUserInfo() {
        if (SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID) != null) {
            userID = SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID);
        }
        if (SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USER_Profile_Picture) != null) {
            String strProfile_Image = SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USER_Profile_Picture);
            if (!strProfile_Image.equalsIgnoreCase("")) {
                imgUser.setVisibility(View.GONE);
                circularImageViewUser.setVisibility(View.VISIBLE);
                try {
                    picUrl = String.valueOf(strProfile_Image);
                    urla = new URL(picUrl);
                    urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(), urla.getPath(), urla.getQuery(), urla.getRef());
                    picUrl = String.valueOf(urin.toURL());
                    Picasso.get()
                            .load(picUrl)
                            .into(circularImageViewUser, new Callback() {
                                @Override
                                public void onSuccess() {
                                    //holder.pbHome.setVisibility(View.INVISIBLE);
                                }

                                @Override
                                public void onError(Exception e) {
                                    //holder.pbHome.setVisibility(View.INVISIBLE);
                                }
                            });
                } catch (Exception e) {
                    e.printStackTrace();
                    //Creating SendMail object
                    SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in MainActivity.java When parsing image url\n" + e.toString());
                    //Executing sendmail to send email
                    sm.execute();
                }
            } else {
                imgUser.setVisibility(View.VISIBLE);
                circularImageViewUser.setVisibility(View.GONE);
            }
        } else {
            imgUser.setVisibility(View.VISIBLE);
            circularImageViewUser.setVisibility(View.GONE);
        }

        if (SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USER_EMAIL) != null) {
            txtUserEmail.setText(SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USER_EMAIL));
        } else {
            txtUserEmail.setText(R.string.guest_email_address);
        }

        if (SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USER_Name) != null) {
            txtUserName.setText(SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USER_Name));
        } else {
            txtUserName.setText(R.string.guest_user);
        }
    }//endregion

    public void clearBackStack() {
        FragmentManager manager = getSupportFragmentManager();
        if (manager.getBackStackEntryCount() > 0) {
            FragmentManager.BackStackEntry first = manager.getBackStackEntryAt(0);
            manager.popBackStack(first.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    public static void BestSellingListingRefresh(String strProductID, String strType) {
        if (bestSellingProductLists != null) {
            for (int i = 0; i < bestSellingProductLists.size(); i++) {
                if (strProductID.equalsIgnoreCase(bestSellingProductLists.get(i).getProductId())) {
                    bestSellingProductLists.get(i).setExists_in_wishlist(strType);
                }
            }
            recyclerviewBestSelling.getAdapter().notifyDataSetChanged();
        }
    }

    public static void MostWantedListingRefresh(String strProductID, String strType) {
        if (MostWantedProductLists != null) {
            for (int i = 0; i < MostWantedProductLists.size(); i++) {
                if (strProductID.equalsIgnoreCase(MostWantedProductLists.get(i).getProductId())) {
                    MostWantedProductLists.get(i).setExists_in_wishlist(strType);
                }
            }
            recyclerviewMostwanted.getAdapter().notifyDataSetChanged();
        }
    }

    public static void NewArrivalListingRefresh(String strProductID, String strType) {
        if (NewArrivalProductLists != null) {
            for (int i = 0; i < NewArrivalProductLists.size(); i++) {
                if (strProductID.equalsIgnoreCase(NewArrivalProductLists.get(i).getProductId())) {
                    NewArrivalProductLists.get(i).setExists_in_wishlist(strType);
                }
            }
            recyclerviewNewArrivals.getAdapter().notifyDataSetChanged();
        }
    }

    public void openGPlus(String id) {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setClassName("com.google.android.apps.plus",
                    "com.google.android.apps.plus.phone.UrlGatewayActivity");
            intent.putExtra("customAppUri", id);
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://plus.google.com/" + id)));
        }
    }

    public void openOurProduct() {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.gamebitco.com/")));

    }

    private void openFacebookApp() {
        String facebookUrl = "https://www.facebook.com/CommanCarry-415403602270210";
        String facebookID = "CommanCarry-415403602270210";
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(facebookUrl)));
    }

}
