package com.pureites.commancarry.Model;

/**
 * Created by divya on 13/9/17.
 */

public class ProductList {
    String Productname;
    String Productprice;
    String Productsale_price;
    String ProductId;
    String main_image;
    String slug;
    String exists_in_wishlist;
    String Rating;
    String start_datetime;
    String expire_datetime;
    String dealprice;
    String currentdatetime;

    public ProductList(String productname, String productprice, String productsale_price,
                       String productId, String main_image, String slug,
                       String exists_in_wishlist, String rating, String start_datetime,
                       String expire_datetime, String dealprice, String currentdatetime) {
        Productname = productname;
        Productprice = productprice;
        Productsale_price = productsale_price;
        ProductId = productId;
        this.main_image = main_image;
        this.slug = slug;
        this.exists_in_wishlist = exists_in_wishlist;
        Rating = rating;
        this.start_datetime = start_datetime;
        this.expire_datetime = expire_datetime;
        this.dealprice = dealprice;
        this.currentdatetime = currentdatetime;
    }

    public ProductList(String productname, String productprice, String productsale_price, String productId,
                       String main_image, String slug, String exists_in_wishlist, String Rating) {
        Productname = productname;
        Productprice = productprice;
        Productsale_price = productsale_price;
        ProductId = productId;
        this.main_image = main_image;
        this.slug = slug;
        this.exists_in_wishlist = exists_in_wishlist;
        this.Rating = Rating;
    }

    public String getProductname() {
        return Productname;
    }

    public String getProductprice() {
        return Productprice;
    }

    public String getProductsale_price() {
        return Productsale_price;
    }

    public String getProductId() {
        return ProductId;
    }

    public String getMain_image() {
        return main_image;
    }

    public String getSlug() {
        return slug;
    }

    public String getExists_in_wishlist() {
        return exists_in_wishlist;
    }

    public String getRating() {
        return Rating;
    }

    public void setProductname(String productname) {
        Productname = productname;
    }

    public void setProductprice(String productprice) {
        Productprice = productprice;
    }

    public void setProductsale_price(String productsale_price) {
        Productsale_price = productsale_price;
    }

    public void setProductId(String productId) {
        ProductId = productId;
    }

    public void setMain_image(String main_image) {
        this.main_image = main_image;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public void setExists_in_wishlist(String exists_in_wishlist) {
        this.exists_in_wishlist = exists_in_wishlist;
    }

    public void setRating(String rating) {
        Rating = rating;
    }

    public String getStart_datetime() {
        return start_datetime;
    }

    public void setStart_datetime(String start_datetime) {
        this.start_datetime = start_datetime;
    }

    public String getExpire_datetime() {
        return expire_datetime;
    }

    public void setExpire_datetime(String expire_datetime) {
        this.expire_datetime = expire_datetime;
    }

    public String getDealprice() {
        return dealprice;
    }

    public void setDealprice(String dealprice) {
        this.dealprice = dealprice;
    }

    public String getCurrentdatetime() {
        return currentdatetime;
    }

    public void setCurrentdatetime(String currentdatetime) {
        this.currentdatetime = currentdatetime;
    }
}
