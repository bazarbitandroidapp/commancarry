package com.pureites.commancarry.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pureites.commancarry.Global.Global;
import com.pureites.commancarry.Global.SharedPreference;
import com.pureites.commancarry.Global.StaticUtility;
import com.pureites.commancarry.Global.Typefaces;
import com.pureites.commancarry.Model.Category;
import com.pureites.commancarry.Model.DrawerItem;
import com.pureites.commancarry.R;

import java.util.ArrayList;

public class DrawerItemsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private ArrayList<DrawerItem> drawerItems = new ArrayList<>();
    DrawerCategoryOption drawerCategoryOption;

    public DrawerItemsAdapter(Context mContext, ArrayList<DrawerItem> drawerItems) {
        this.mContext = mContext;
        this.drawerItems = drawerItems;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_drawer, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            ViewHolder viewHolder = (ViewHolder) holder;
            final DrawerItem drawerItem = drawerItems.get(position);
            /*if (position == 0) {
                viewHolder.imgCMS.setVisibility(View.VISIBLE);
                viewHolder.mTxtCategoryLable.setVisibility(View.VISIBLE);
            } else {
                viewHolder.imgCMS.setVisibility(View.INVISIBLE);
                viewHolder.mTxtCategoryLable.setVisibility(View.GONE);
            }*/
            viewHolder.txtCMS.setText(drawerItem.getName());

            if (drawerItem.isSelect()) {
                if (drawerItem.getSlug().equalsIgnoreCase("info") ||
                        drawerItem.getSlug().equalsIgnoreCase("category")) {
                    viewHolder.llSelection.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                    viewHolder.llSelection.setVisibility(View.VISIBLE);
                    viewHolder.mViewTopDivider.setVisibility(View.VISIBLE);
                    viewHolder.mViewBottomDivider.setVisibility(View.VISIBLE);
                } else {
                    viewHolder.mViewTopDivider.setVisibility(View.GONE);
                    viewHolder.mViewBottomDivider.setVisibility(View.GONE);
                    viewHolder.txtCMS.setTextSize(mContext.getResources().getDimension(R.dimen.txtmenu_select_textSize) / mContext.getResources().getDisplayMetrics().density);
                    viewHolder.llSelection.setVisibility(View.VISIBLE);
                    viewHolder.llCMS.setBackgroundColor(mContext.getResources().getColor(R.color.selectionColor));
                    viewHolder.llSelection.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                    viewHolder.txtCMS.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                    viewHolder.imgCMS.setColorFilter(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                }
            } else {
                if (drawerItem.getSlug().equalsIgnoreCase("info")||
                        drawerItem.getSlug().equalsIgnoreCase("category")) {
                    viewHolder.llSelection.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                    viewHolder.llSelection.setVisibility(View.VISIBLE);
                    viewHolder.mViewTopDivider.setVisibility(View.VISIBLE);
                    viewHolder.mViewBottomDivider.setVisibility(View.VISIBLE);
                } else {
                    viewHolder.mViewTopDivider.setVisibility(View.GONE);
                    viewHolder.mViewBottomDivider.setVisibility(View.GONE);
                    viewHolder.llCMS.setBackgroundColor(mContext.getResources().getColor(R.color.colorcard));
                    viewHolder.txtCMS.setTextSize(mContext.getResources().getDimension(R.dimen.txtmenu_textSize) / mContext.getResources().getDisplayMetrics().density);
                    viewHolder.llSelection.setVisibility(View.GONE);
                    viewHolder.txtCMS.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                    viewHolder.imgCMS.setColorFilter(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                }
            }

            viewHolder.llCMS.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    drawerCategoryOption = (DrawerCategoryOption) mContext;
                    drawerCategoryOption.redirection(drawerItem);
                    for (int i = 0; i < drawerItems.size(); i++) {
                        DrawerItem drawerItem = drawerItems.get(i);
                        drawerItem.setSelect(false);
                    }
                    drawerItem.setSelect(true);
                    notifyDataSetChanged();
                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return drawerItems.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imgCMS;
        TextView txtCMS;
        LinearLayout llCMS;
        LinearLayout llSelection, mLlMain;
        View mViewTopDivider, mViewBottomDivider;

        public ViewHolder(View itemView) {
            super(itemView);
            llCMS = (LinearLayout) itemView.findViewById(R.id.llCMS);
            imgCMS = (ImageView) itemView.findViewById(R.id.imgCMS);
            txtCMS = (TextView) itemView.findViewById(R.id.txtCMS);
            llSelection = (LinearLayout) itemView.findViewById(R.id.llSelection);
            mLlMain = (LinearLayout) itemView.findViewById(R.id.llMain);
            mViewTopDivider = itemView.findViewById(R.id.viewTopDivider);
            mViewBottomDivider = itemView.findViewById(R.id.viewBottomDivider);

            txtCMS.setTypeface(Typefaces.TypefaceCalibri_Regular(mContext));
            txtCMS.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));

        }
    }

    public interface DrawerCategoryOption {
        void redirection(DrawerItem drawerItem);
    }
}
