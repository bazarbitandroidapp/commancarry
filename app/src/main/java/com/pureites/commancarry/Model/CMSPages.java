package com.pureites.commancarry.Model;

/**
 * Created by divya on 10/4/18.
 */

public class CMSPages {
    String title;
    String slug;

    public CMSPages(String title, String slug) {
        this.title = title;
        this.slug = slug;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }
}
