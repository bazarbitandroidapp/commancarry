package com.pureites.commancarry.Model;

public class DrawerItem {
    String Name = "";
    String Slug = "";
    String Type = "";
    boolean isSelect;

    public DrawerItem(String name, String slug, String type) {
        Name = name;
        Slug = slug;
        Type = type;
        this.isSelect = isSelect;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getSlug() {
        return Slug;
    }

    public void setSlug(String slug) {
        Slug = slug;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }
}
